#!/usr/bin/python
# coding=utf-8
from __future__ import division
from math import log
import numpy as np
import pandas as pd
from scipy import special


class GenerateRTB():
    limit = 500

    def generateRTB(self, df, cols):
        return


class GenerateRTB_BinClass(GenerateRTB):
    def generateRTB(self, df,dftest, target, tarnum, cols=None):
        if cols == None:
            cols = df.columns

        for v in cols:
            gb = df.groupby(by=[v, target]).size()
            gb = gb.reset_index()
            gb = gb[gb[target] == tarnum]
            gb.rename(columns={0: 'size1'}, inplace=True)
            gbv = df.groupby(by=[v]).size()
            gbv = gbv.reset_index()
            gbv.rename(columns={0: 'size2'}, inplace=True)
            merge = pd.merge(gb, gbv, on=v, how='left')
            merge[v + 'rtb'] = merge['size1']/merge['size2']
            merge = merge.ix[:,[v,v + 'rtb']]
            df = pd.merge(df,merge,on=v,how='left')
            dftest = pd.merge(dftest,merge,on=v,how='left')

        return df,dftest



class SmoothandRTBBeta(GenerateRTB):
    def smooth(self, df, cols, impcl):
        imp = impcl[0]
        cl = impcl[1]
        for v in cols:
            gball = df.groupby(by=v).agg({imp: np.sum, cl: np.sum})
            gball = gball.reset_index()
            # alpa = gball[cl].mean()
            # beta = gball[imp].mean()
            alpa = 1
            beta = 20
            gb = gball[gball[imp] > 0]
            k = 0
            while True:

                gb['fenzi'] = gb[cl].apply(lambda x: special.digamma(x + alpa) - special.digamma(alpa))
                gb['fenzi1'] = gb.apply(lambda x: special.digamma(x[imp] - x[cl] + beta) - special.digamma(beta),
                                        axis=1)

                gb['fenmu'] = gb[imp].apply(lambda x: special.digamma(x + alpa + beta) - special.digamma(alpa + beta))
                fenmu = gb['fenmu'].sum()

                alpa = alpa * (gb['fenzi'].sum() / fenmu)
                beta = beta * (gb['fenzi1'].sum() / fenmu)
                k = k + 1
                print alpa, beta
                if k > 1000:
                    break

            gball[v + cl] = gball[cl] + alpa
            gball[v + imp] = gball[imp] + alpa + beta
            gball[v + 'ctr'] = gball[cl] / gball[imp]
            del gball[cl], gball[imp]
            df = pd.merge(df, gball, left_on=v, right_on=v, how='left')

        return df


if __name__ == '__main__':
    smooth = SmoothImpClBeta()
    df = pd.read_table('D:/1.tsv')
    df.describe()
    smooth.smooth(df, [['creativeid']], ['imp', 'cl'])
