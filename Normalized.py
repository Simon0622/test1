#!/usr/bin/python
# coding=utf-8
from __future__ import division
from math import log
import numpy as np
import pandas as pd
from math import log


class Normalize():

    def normalize(self, df, cols):
        return


class NormalizeA(Normalize):
    def normalize(self, df, onehotargs,target=None):
        for col in df.columns:
            if col not in onehotargs:
                if col == target:
                    continue
                min = df[col].min()
                max = df[col].max()
                df[col] = (df[col]-min)/(max-min)
        return df

class NormalizeB(Normalize):
    def normalize(self, df, onehotargs,target):
        for col in df.columns:
            if col not in onehotargs:
                df[col] = df[col].apply(lambda x:log(x))
                min = df[col].min()
                max = df[col].max()
                df[col] = (df[col]-min)/(max-min)
        return df




if __name__ == '__main__':
    nor = Normalize()
    df = pd.read_csv('D:/paper/csample.csv')
    nor.normalize(df,[])
