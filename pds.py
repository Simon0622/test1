mltgb['histcount'] = mltgb.apply(lambda x:mltgb[(mltgb['ts']<x['ts'])&(mltgb['Location_id']==x['Location_id'])&(mltgb['Merchant_id']==x['Merchant_id'])].shape[0],axis=1)


mltgb[(mltgb['ts']<=(x['ts']- datetime.timedelta(7)))&(mltgb['Merchant_id']==x['Merchant_id'])&(mltgb['Location_id']==x['Location_id'])].sort_values('ts',ascending=False).head(1)['ts']


mltgb['7daysbf'] = mltgb.apply(lambda x:mltgb[(mltgb['ts']<=(x['ts']- datetime.timedelta(7)))&(mltgb['Merchant_id']==x['Merchant_id'])&(mltgb['Location_id']==x['Location_id'])].sort_values('ts',ascending=False).head(1)['ts'].get_values()[0],axis=1)


u'User_id', u'Location_id', u'lastday_', u'ts_x', u'l_belongto_m',
       u'm_belongto_loc', u'7daysbf', u'30daysbf', u'Merchant_id', u'label',
       u'diffdays', u'count_x', u'lnew_u_cnt_y_x', u'mnew_u_cnt_x',
       u'mhistcount_x', u'lhistcount_x', u'mlnew_u_cnt_x', u'mltcount_x',
       u'mcount_x', u'mlthistcount_x', u'lcount_x', u'ts_y', u'count_y',
       u'lnew_u_cnt_y_y', u'mnew_u_cnt_y', u'mhistcount_y', u'lhistcount_y',
       u'mlnew_u_cnt_y', u'mltcount_y', u'mcount_y', u'mlthistcount_y',
       u'lcount_y', u'ts', u'count', u'lnew_u_cnt_y', u'mnew_u_cnt',
       u'mhistcount', u'lhistcount', u'mlnew_u_cnt', u'mltcount', u'mcount',
       u'mlthistcount', u'lcount'

cross['avgbgt'] = cross['Budget']/cross['l_belongto_m']
cross.rename(columns={'lnew_u_cnt_y_x':'lnew_u_cnt_x','lnew_u_cnt_y_y':'lnew_u_cnt_y','lnew_u_cnt_y':'lnew_u_cnt','mnew_u_cnt_x':'mnew_u_cnt_today','mhistcount_x':'mhistcount_today','lhistcount_x':'lhistcount_today','mlnew_u_cnt_x':'mlnew_u_cnt_today','mltcount_x':'mltcount_today','mcount_x':'mcount_today','mlthistcount_x':'mlthistcount_today','lcount_x':'lcount_today'},inplace=True)



u'User_id', u'Location_id', u'lastday_', u'ts_x', u'l_belongto_m',
       u'm_belongto_loc', u'7daysbf', u'30daysbf', u'Merchant_id', u'label',
       u'diffdays', u'lnew_u_cnt_x', u'mnew_u_cnt_today', u'mhistcount_today',
       u'lhistcount_today', u'mlnew_u_cnt_today', u'mltcount_today',
       u'mcount_today', u'mlthistcount_today', u'lcount_today',
       u'lnew_u_cnt_y', u'mnew_u_cnt_y', u'mhistcount_y', u'lhistcount_y',
       u'mlnew_u_cnt_y', u'mltcount_y', u'mcount_y', u'mlthistcount_y',
       u'lcount_y', u'lnew_u_cnt', u'mnew_u_cnt', u'mhistcount', u'lhistcount',
       u'mlnew_u_cnt', u'mltcount', u'mcount', u'mlthistcount', u'lcount'

cross.rename(columns={'lnew_u_cnt_x':'lnew_u_cnt_today','lnew_u_cnt_y':'lnew_u_cnt_1daybf','mnew_u_cnt_y':'mnew_u_cnt_1daybf','mhistcount_y':'mhistcount_1daybf','lhistcount_y':'lhistcount_1daybf','mlnew_u_cnt_y':'mlnew_u_cnt_1daybf','mltcount_y':'mltcount_1daybf','mcount_y':'mcount_1daybf','mlthistcount_y':'mlthistcount_1daybf','lcount_y':'lcount_1daybf','lnew_u_cnt':'lnew_u_cnt_7daysbf','mnew_u_cnt':'mnew_u_cnt_7daysbf','mhistcount':'mhistcount_7daysbf','lhistcount':'lhistcount_7daysbf','mlnew_u_cnt':'mlnew_u_cnt_7daysbf','mltcount':'mltcount_7daysbf','mcount':'mcount_7daysbf','mlthistcount':'mlthistcount_7daysbf','lcount':'lcount_7daysbf'},inplace=True)

u'User_id', u'Location_id', u'lastday_', u'ts_x', u'l_belongto_m',
       u'm_belongto_loc', u'7daysbf', u'30daysbf', u'Merchant_id', u'label',
       u'diffdays', u'lnew_u_cnt_today', u'mnew_u_cnt_today',
       u'mhistcount_today', u'lhistcount_today', u'mlnew_u_cnt_today',
       u'mltcount_today', u'mcount_today', u'mlthistcount_today',
       u'lcount_today', u'lnew_u_cnt_1daybf', u'mnew_u_cnt_1daybf',
       u'mhistcount_1daybf', u'lhistcount_1daybf', u'mlnew_u_cnt_1daybf',
       u'mltcount_1daybf', u'mcount_1daybf', u'mlthistcount_1daybf',
       u'lcount_1daybf', u'lnew_u_cnt_7daysbf', u'mnew_u_cnt_7daysbf',
       u'mhistcount_7daysbf', u'lhistcount_7daysbf', u'mlnew_u_cnt_7daysbf',
       u'mltcount_7daysbf', u'mcount_7daysbf', u'mlthistcount_7daysbf',
       u'lcount_7daysbf'

part['mlhisperday'] = part['mlthistcount_today']-part['mlthistcount_1daybf']
part['mlhisperweek'] = part['mlthistcount_today']-part['mlthistcount_7daysbf']
part['dayratio'] = part['mlhisperday']/part['mlthistcount_today']
part['weekratio'] = part['mlhisperweek']/part['mlthistcount_today']

part['mhisperday'] = part['mhistcount_today']-part['mhistcount_1daybf']
part['mhisperweek'] = part['mhistcount_today']-part['mhistcount_7daysbf']
part['mdayratio'] = part['mhisperday']/part['mhistcount_today']
part['mweekratio'] = part['mhisperweek']/part['mhistcount_today']

part['lhisperday'] = part['lhistcount_today']-part['lhistcount_1daybf']
part['lhisperweek'] = part['lhistcount_today']-part['lhistcount_7daysbf']
part['ldayratio'] = part['lhisperday']/part['lhistcount_today']
part['lweekratio'] = part['lhisperweek']/part['lhistcount_today']

part['ml_m_ratio'] = part['mlthistcount_today']/part['mhistcount_today']
part['ml_l_ratio'] = part['mlthistcount_today']/part['lhistcount_today']

part['new_all_ratio_ml'] = part['mlnew_u_cnt_today']/part['mlthistcount_today']
part['new_old_ratio_ml'] = part['mlnew_u_cnt_today']/part['mltcount_today']
part['new_old_ratio_m'] = part['mnew_u_cnt_today']/part['mcount_today']
part['new_old_ratio_l'] = part['lnew_u_cnt_today']/part['lcount_today']

part['new_old_ratio_ml1df'] = part['mlnew_u_cnt_1daybf']/part['mltcount_1daybf']
part['new_old_ratio_m1df'] = part['mnew_u_cnt_1daybf']/part['mcount_1daybf']
part['new_old_ratio_l1df'] = part['lnew_u_cnt_1daybf']/part['lcount_1daybf']

part['new_old_ratio_ml7df'] = part['mlnew_u_cnt_7daysbf']/part['mltcount_7daysbf']
part['new_old_ratio_m7df'] = part['mnew_u_cnt_7daysbf']/part['mcount_7daysbf']
part['new_old_ratio_l7df'] = part['lnew_u_cnt_7daysbf']/part['lcount_7daysbf']

part['newu_ml_m'] = part['mlnew_u_cnt_today']/part['mnew_u_cnt_today']
part['newu_ml_l'] = part['mlnew_u_cnt_today']/part['lnew_u_cnt_today']

part['newu_1dbfnu'] = part['mlnew_u_cnt_1daybf']/part['mlnew_u_cnt_today']   ####用最新一天防止除数为0
part['newu_7dbfnu'] = part['mlnew_u_cnt_7daysbf']/part['mlnew_u_cnt_today']

part['avgperday'] = part['mlthistcount_today']/part['diffdays']
part['locavgtoday'] = part['lhistcount_today']/part['m_belongto_loc']












#####每天每个商家loc的新用户数
merge['day_nu'] = merge['ml_u_cnt']-merge['lastday_ml_u_cnt']
###新用户占当天所有的比例
merge['nu_ou'] = merge['day_nu']/merge['mlcount']
###loc_mrt 一天和一周新增用户比例#####
merge['ml_nu_day_ratio'] = (merge['ml_u_cnt']-merge['lastday_ml_u_cnt'])/merge['ml_u_cnt']
merge['ml_nu_week_ratio'] = (merge['ml_u_cnt']-merge['7lastday_ml_u_cnt'])/merge['ml_u_cnt']
###loc_mrt 一天和一周新增访问量比例#####
merge['ml_day_ratio'] = (merge['mlhistcount']-merge['lastday_mlhistcount'])/merge['mlhistcount']
merge['ml_week_ratio'] = (merge['mlhistcount']-merge['7lastday_mlhistcount'])/merge['mlhistcount']
###当天与历史对比###
merge['ml_td_hisratio'] = merge['mlcount']/merge['mlhistcount']
###当天占同loc比例###
merge['ml_l_ratio'] = merge['mlcount']/merge['lcount']

####平均每天访问量
merge['avgday'] = merge['mlhistcount']/merge['m_exist_days']
####今天与平均的比值
merge['avg_add_ratio'] = merge['mlcount']/merge['avgday']
# ###mrt 一天和一周新增用户比例#####
# merge['m_nu_day_ratio'] = (merge['m_u_cnt']-merge['lastday_m_u_cnt'])/merge['m_u_cnt']
# merge['m_nu_week_ratio'] = (merge['m_u_cnt']-merge['7lastday_m_u_cnt'])/merge['m_u_cnt']
# ###mrt 一天和一周新增访问量比例#####
# merge['m_day_ratio'] = (merge['mhistcount']-merge['lastday_mhistcount'])/merge['mhistcount']
# merge['m_week_ratio'] = (merge['mhistcount']-merge['7lastday_mhistcount'])/merge['mhistcount']
#
# ###loc 一天和一周新增用户比例#####
# merge['l_nu_day_ratio'] = (merge['l_u_cnt']-merge['lastday_l_u_cnt'])/merge['l_u_cnt']
# merge['l_nu_week_ratio'] = (merge['l_u_cnt']-merge['7lastday_l_u_cnt'])/merge['l_u_cnt']
# ###loc 一天和一周新增访问量比例#####
# merge['l_day_ratio'] = (merge['lhistcount']-merge['lastday_lhistcount'])/merge['lhistcount']
# merge['l_week_ratio'] = (merge['lhistcount']-merge['7lastday_lhistcount'])/merge['lhistcount']
# del merge['m_u_cnt']
# del merge['l_u_cnt']
# del merge['mhistcount']
# del merge['lhistcount']
# del merge['m_nu_day_ratio']
# del merge['m_nu_week_ratio']
# del merge['m_day_ratio']
# del merge['m_week_ratio']
# del merge['l_nu_day_ratio']
# del merge['l_nu_week_ratio']
# del merge['l_day_ratio']
# del merge['l_week_ratio']




for col in merge.columns:
    if col.startswith('lastday_') or col.startswith('7lastday_'):
        del merge[col]



u'User_id', u'Location_id', u'Merchant_id', u'month', u'lastday',
       u'lastweek', u'ts', u'l_belongto_m', u'm_belongto_loc', u'm_exist_days',
       u'ml_u_cnt', u'm_u_cnt', u'l_u_cnt', u'mltcount', u'mcount', u'lcount',
       u'mlthistcount', u'mhistcount', u'lhistcount', u'ml_l_hisratio',
       u'ml_l_ratio', u'avgday', u'ml_nu_day_ratio', u'ml_nu_week_ratio',
       u'ml_day_ratio', u'ml_week_ratio', u'm_nu_day_ratio',
       u'm_nu_week_ratio', u'm_day_ratio', u'm_week_ratio', u'l_nu_day_ratio',
       u'l_nu_week_ratio', u'l_day_ratio', u'l_week_ratio', u'avg_add_ratio'





u'User_id', u'Location_id', u'Merchant_id', u'month', u'lastday',
       u'lastweek', u'ts', u'l_belongto_m', u'm_belongto_loc', u'm_exist_days',
       u'ml_u_cnt', u'mltcount', u'mcount', u'lcount', u'mlthistcount',
       u'ml_l_hisratio', u'ml_l_ratio', u'avgday', u'ml_nu_day_ratio',
       u'ml_nu_week_ratio', u'ml_day_ratio', u'ml_week_ratio',
       u'avg_add_ratio', u'ml_td_hisratio'



























']
