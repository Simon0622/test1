#!/usr/bin/python
# coding=utf-8
from math import log
from __future__ import division
import numpy as np
import pandas as pd
from scipy import special
import pickle
import json
from scipy.sparse import csr_matrix
import xgboost as xgb


class Format():
    limit = 500
    map = {}

    def __init__(self, map):
        self.map = map

    def onehot_coding(self, dataframe, params, target):
        map = self.map
        k = -1
        for col in dataframe.columns:
            if col in params:
                list = dataframe[col].unique().tolist()
                map[col] = {}
                for l in list:
                    if pd.isnull(l):
                        continue
                    else:
                        k += 1
                        map[col][l] = k
            elif col == target:
                continue
            elif col == 'ctr':
                continue
            else:
                k += 1
                map[col] = k

        # write_map(map, dataframe)
        return map

    def pickle_obj(self, obj, name):
        pickle.dump(obj, open(name, "w"))

    def load_obj(self, name):
        obj = pickle.load(open(name, "r"))
        return obj

    def json_obj(self, obj, name):
        with open(name, 'w') as f:
            f.write(json.dumps(obj))

    def json_load(self, name):
        with open(name, 'r') as f:
            data = json.load(f)
            return data


class FormatFFM(Format):
    def format(self, df, onehotcolumns, fields, name, target):
        map = self.onehot_coding(df, onehotcolumns, target)
        self.json_obj(map, 'D:/ffm.map')
        self.write_ffm_file(df, map, fields, name, target)

    def write_ffm_file(self, df, map, fields, name, target=None):
        f = 0
        fieldmap = {}
        ##方便根据col查询其对应的field
        for k, v in fields.items():
            for col in v:
                fieldmap[col] = f
            f = f + 1

        fo = open(name, 'w')
        for idx, row in df.iterrows():
            rowstr = ''
            for k,v in fields.items():
                for col in v:
                    if map.get(col, 'hehe') != 'hehe':
                        fieldkey = fieldmap.get(col)
                        value = row[col]
                        submap = map.get(col)
                        if isinstance(submap, dict):
                            ###是需要one-hot编码的列，索引是列名加上对应的取值 如width=600 对应一个数
                            if submap.get(value, 'hehe') != 'hehe':
                                kval = submap.get(value)
                                gstr = str(fieldkey) + ":" + str(kval) + ":1"
                                rowstr = rowstr + ' ' + gstr
                            else:
                                #####one-hot编码，但是map中没有找到对应取值，对应missing值
                                continue
                        else:
                            gstr = str(fieldkey) + ":" + str(submap) + ':' + str(value)
                            rowstr = rowstr + ' ' + gstr
                    else:
                        ##异常，没有找到map中对应的列名
                        continue
            if target is not None:
                fo.write(str(row[target]) + rowstr + '\n')
            else:
                fo.write(rowstr + '\n')


class FormatGBDT(Format):
    def format(self, df, onehotcolumns, target, filename=None):
        map = format.onehot_coding(train, ['User_id','Location_id','Merchant_id','ts'], 'label')
        format.pickle_obj(map, 'gbdt.map')
        format.write_fmap(map, train, 'label', 'train.fmap')
        format.load_map('test.xgb', test, map, 'label')
        format.load_map('train.xgb', train, map, 'label')
        csr, labels = format.to_scipy_sparse(train, map,'label')
        self.train(csr)

    def write_fmap(self, nmap, dataframe, target, name):
        fo = open(name, 'w')
        for param in dataframe.columns:
            if param == target or param == 'ctr':
                continue
            if isinstance(nmap[param], dict):
                subdict = nmap[param]
                for (k, v) in sorted(subdict.items(), lambda x, y: cmp(x[1], y[1])):
                    fo.write(str(v) + "\t" + str(param) + "=" + str(k) + "\ti\n")
            else:
                fo.write(str(nmap[param]) + "\t" + param + "\tq\n")
        fo.close()

    def load_map(self, name, df, map, target):
        fo = open(name, 'w')
        for idx, row in df.iterrows():
            rowstr = ''
            for col in df.columns:
                if map.get(col, 'hehe') != 'hehe':
                    value = row[col]
                    submap = map.get(col)
                    if isinstance(submap, dict):
                        if submap.get(value, 'hehe') != 'hehe':
                            kval = submap.get(value)
                            rowstr = rowstr + ' ' + str(kval) + ':1'
                        else:
                            continue
                    else:
                        rowstr = rowstr + ' ' + str(submap) + ':' + str(value)
                else:
                    continue
            fo.write(str(row[target]) + rowstr + '\n')

    def train(self, train_dat, test_dat, labels=None, param=None, num_round=None):
        if num_round == None:
            num_round = 50
        if param == None:
            param = {'max_depth': 3, 'eta': 0.1, 'objective': 'binary:logistic'}
        dtrain = xgb.DMatrix(train_dat, label=labels)
        dtest = xgb.DMatrix(test_dat)
        label = dtrain.get_label()
        ratio = float(np.sum(label == 0)) / np.sum(label == 1)
        param['scale_pos_weight'] = ratio
        watchlist  = [(dtest,'eval'), (dtrain,'train')]
        # result = xgb.cv(param, dtrain, num_round, nfold=5,metrics={'auc'}, seed = 0, show_stdv = False)
        # print result
        bst = xgb.train(param, dtrain, num_round)
        bst.dump_model('dump.nice.txt','titanic/titanic.fmap')
        preds = bst.predict(dtest)
        return preds
        # s1 = pd.Series(preds)
        # s1 = s1.apply(lambda x:int(x>0.5))
        # s1.to_csv('result.csv',index=False)

    def sparse(train,test,onehots,target):
        cols = train.columns
        dens_feat = list(set(cols)^set(onehots))
        dens_feat = list(set(dens_feat)^set([target]))

        train_array = train[dens_feat].as_matrix()
        test_array = test[dens_feat].as_matrix()
        if target in test.columns:
            return train_array,train[target],test_array,test[target]
        else:
            return train_array,train[target],test_array



    def to_scipy_sparse(self, df, map, target = None):
        rowl = []
        coll = []
        data = []
        labels = []
        rows = -1
        for idx, row in df.iterrows():
            rows = rows + 1
            if target is not None:
                labels.append(int(row[target]))
            for col in df.columns:
                if col == target:
                    continue
                if map.get(col, 'hehe') != 'hehe':
                    value = row[col]
                    submap = map.get(col)
                    if isinstance(submap, dict):
                        ###one-hot特征
                        if submap.get(value, 'hehe') != 'hehe':
                            kval = submap.get(value)
                            rowl.append(rows)
                            coll.append(kval)
                            data.append(1)
                        else:
                            continue
                    else:
                        ###实值特征
                        # rowstr = rowstr + ' ' + str(submap) + ':' + str(value)
                        rowl.append(rows)
                        coll.append(submap)
                        data.append(value)
                else:
                    continue

        csr = csr_matrix((data, (rowl, coll)))
        return csr, labels


if __name__ == '__main__':
    format = FormatGBDT({})
    df = pd.read_table('D:/3hour_jtmodel.tsv.bak')
    format.format(df, ['appcategoryid', 'height', 'width', 'platform', 'slottoken', 'displaytype', 'sspid', 'modeltype',
                       'devicetype', 'province_id', 'city_id', 'fx', 'creativeid'],
                  {'device': ['width', 'height', 'appcategoryid', 'isjailbroken', 'platform', 'modeltype', 'devicetype',
                              'province_id', 'city_id', 'fx', 'imp', 'cl', 'cheatcl'],
                   'publish': ['slottoken', 'displaytype', 'sspid'],
                   'ad': ['creativeid']},
                  'D:/ffm_dat',
                  'ctr')

    tr_X,tr_y,te_X,te_y = sparse(train,test,['User_id','Location_id','Merchant_id','ts','lastday','lastweek','mstartday'],'label')
    param_dist = {'objective':'binary:logistic', 'n_estimators':30,'max_depth':7, 'learning_rate':1, 'silent':1}
    clf = xgb.XGBClassifier(**param_dist)
    clf.fit(tr_X, tr_y,
            eval_set=[(tr_X, tr_y), (te_X, te_y)],
            eval_metric='error',
            verbose=True)
