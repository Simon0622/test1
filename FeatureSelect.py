#!/usr/bin/python
#coding=utf-8
from __future__ import division
from math import log
import numpy as np
import pandas as pd
from sklearn.feature_selection import SelectFromModel
from sklearn.ensemble import ExtraTreesClassifier


class FeatureSelect():

    def feature_select(self, df, cols):
        return


class FeatureSelectEnt(FeatureSelect):

    def feature_select(self, df, cols, target):
        # 计算每一列的条件熵
        shape = df.shape
        tsum = shape[0]
        colEnt = {}
        for col in cols:
            colcoEnt = self.computeConEnt(col, tsum, df, target)
            colEnt[col] = colcoEnt

        colEnt = self.sortDic(colEnt,0)
        print str(colEnt)

    def sortDic(Dict):
        return sorted(Dict.items(),key=lambda e:e[1],reverse=True)  #reverse=True 倒排


    def getEnt(self, pargs):
        sum = 0
        for i in pargs:
            sum += i * log(i, 2)
        return -sum

    def computeConEnt(self, col, tsum, df, target):
        # 求条件熵
        gb = df.groupby(by=[col, target]).size()
        gb = gb.reset_index()
        gb.rename(columns={0: 'size'}, inplace=True)
        #列出该列所有取值
        nodelist = df[col].unique().tolist()
        sumConEnt = 0
        for i in nodelist:
            gbi = gb[gb[col] == i]
            sum = gbi['size'].sum()
            pi = sum / tsum
            gbi['cpi'] = gbi['size'] / sum
            l = [gbi['cpi']]
            pargs = np.array(l, dtype=pd.Series)
            conEnt = self.getEnt(pargs[0])
            sumConEnt = sumConEnt + conEnt * pi

        return sumConEnt


class FeatureSelectSklearn(FeatureSelect):
    def feature_select(self, df, cols, target):


if __name__ == '__main__':
    featureSelect = FeatureSelectA()
    
