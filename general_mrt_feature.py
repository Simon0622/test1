#!/usr/bin/python
# coding=utf-8
import pandas as pd
import numpy as np
import datetime
import pickle
import xgboost as xgb
from sklearn.ensemble import RandomForestClassifier
from sklearn.externals import joblib
import random

path = '/Users/simon/Downloads/tianchi'

def split(kb,mrt,ts,ts5,ts10,startts):
	kbtrain = kb[(kb['ts']<=ts)&(kb['ts']>=startts)]

	kbtrain['mstartday'] = kbtrain['mstartday'].apply(lambda x:datetime.datetime.strptime(x,'%Y-%m-%d'))
	kbtrain['tmp'] = datetime.datetime.strptime(ts,'%Y-%m-%d')-kbtrain['mstartday']
	kbtrain['m_exist_days'] = kbtrain['tmp'].apply(lambda x:(x / np.timedelta64(1, 'D')).astype(int)+1)
	del kbtrain['tmp']

	#####kb train集
	ugbtrain = kbtrain.groupby(['Merchant_id','Location_id','User_id']).size()
	ugbtrain = ugbtrain.reset_index()
	ugbtrain.rename(columns={0:'histcnt'},inplace=True)
	ugbtrain['uhistcnt'] = 1
	ugbtrain = ugbtrain.groupby(['Merchant_id','Location_id']).agg({'histcnt':np.sum,'uhistcnt':np.sum})
	ugbtrain = ugbtrain.reset_index()

	ugbtrain5 = kbtrain[kbtrain['ts']<=ts5].groupby(['Merchant_id','Location_id','User_id']).size()
	ugbtrain5 = ugbtrain5.reset_index()
	ugbtrain5.rename(columns={0:'5histcnt'},inplace=True)
	ugbtrain5['5histucnt'] = 1
	ugbtrain5 = ugbtrain5.groupby(['Merchant_id','Location_id']).agg({'5histcnt':np.sum,'5histucnt':np.sum})
	ugbtrain5 = ugbtrain5.reset_index()

	ugbtrain10 = kbtrain[kbtrain['ts']<=ts10].groupby(['Merchant_id','Location_id','User_id']).size()
	ugbtrain10 = ugbtrain10.reset_index()
	ugbtrain10.rename(columns={0:'10histcnt'},inplace=True)
	ugbtrain10['10histucnt'] = 1
	ugbtrain10 = ugbtrain10.groupby(['Merchant_id','Location_id']).agg({'10histcnt':np.sum,'10histucnt':np.sum})
	ugbtrain10 = ugbtrain10.reset_index()

	ugbtrain = pd.merge(ugbtrain,ugbtrain5,on=['Merchant_id','Location_id'],how='left')
	ugbtrain = pd.merge(ugbtrain,ugbtrain10,on=['Merchant_id','Location_id'],how='left')
	####有些商家上线没有5天，因此关联会出现nan值，赋予0比较合适
	ugbtrain = ugbtrain.fillna(0)
	ugbtrain['near5day'] = ugbtrain['histcnt']-ugbtrain['5histcnt']
	ugbtrain['near5uday'] = ugbtrain['uhistcnt']-ugbtrain['5histucnt']
	ugbtrain['near5_10day'] = ugbtrain['5histcnt']-ugbtrain['10histcnt']
	ugbtrain['near5_10uday'] = ugbtrain['5histucnt']-ugbtrain['10histucnt']
	ugbtrain['510ratio'] = ugbtrain['near5day']/(ugbtrain['near5_10day']+1)
	ugbtrain['510uratio'] = ugbtrain['near5uday']/(ugbtrain['near5_10uday']+1)
	ugbtrain['near5cnturatio'] = ugbtrain['near5uday']/(ugbtrain['near5day']+1)
	ugbtrain['near5_10cnturatio'] = ugbtrain['near5_10uday']/(ugbtrain['near5_10day']+1)


	kbtrainsubcol = kbtrain.groupby(['Location_id','Merchant_id','ts']).agg({'m_exist_days':'max'})
	kbtrainsubcol = kbtrainsubcol.reset_index()
	kbtrainsubcol['buyday'] = 1
	kbtrainsubcol = kbtrainsubcol.groupby(['Location_id','Merchant_id']).agg({'m_exist_days':'max','buyday':np.sum})
	kbtrainsubcol = kbtrainsubcol.reset_index()
	kbtrainsubcol['nobuyday'] = kbtrainsubcol['m_exist_days']-kbtrainsubcol['buyday']
	kbtrainsubcol['buycntratio'] = kbtrainsubcol['buyday']/kbtrainsubcol['m_exist_days']


	mlcount = kbtrain.groupby(['Location_id','Merchant_id','ts']).size()
	mlcount = mlcount.reset_index()
	mlcount.rename(columns={0:'sizemax'},inplace=True)
	mlcount['sizemean'] = mlcount['sizemax']
	mlcountgb = mlcount.groupby(['Location_id','Merchant_id']).agg({'sizemax':'max','sizemean':np.mean})
	mlcountgb = mlcountgb.reset_index()

	kbtrainsubcol = pd.merge(kbtrainsubcol,mlcountgb,on=['Location_id','Merchant_id'],how='left')
	ugbtrain = pd.merge(ugbtrain,kbtrainsubcol,on=['Location_id','Merchant_id'],how='left')
	ugbtrain['avgday_user'] = ugbtrain['uhistcnt']/ugbtrain['m_exist_days']
	ugbtrain['unt_cnt_ratio'] = ugbtrain['uhistcnt']/ugbtrain['histcnt']
	ugbtrain['avgday'] = ugbtrain['histcnt']/ugbtrain['m_exist_days']

	ugbtrain = pd.merge(ugbtrain,mrt,on=['Merchant_id','Location_id'],how='left')
	ugbtrain['bgt'] = ugbtrain['Budget']/100
	ugbtrain['bgt'] = ugbtrain['bgt'].apply(lambda x:round(x))
	ugbtrain_bgt_gb = ugbtrain.groupby(['bgt']).agg({'avgday':np.mean,'avgday_user':np.mean})
	ugbtrain_bgt_gb = ugbtrain_bgt_gb.reset_index()
	ugbtrain_bgt_gb.rename(columns={'avgday':'bgroup_avgday_mean','avgday_user':'bgroup_avgday_user_mean'},inplace=True)
	ugbtrain = pd.merge(ugbtrain,ugbtrain_bgt_gb,on=['bgt'],how='left')
	ugbtrain['avg_bg_ratio'] = ugbtrain['avgday']/ugbtrain['bgroup_avgday_mean']

	lgbtrain = ugbtrain.groupby(['Location_id']).agg({'histcnt':np.sum,'uhistcnt':np.sum})
	lgbtrain = lgbtrain.reset_index()
	lgbtrain.rename(columns={'histcnt':'lhistcnt','uhistcnt':'luhistcnt'},inplace=True)
	ugbtrain = pd.merge(ugbtrain,lgbtrain,on=['Location_id'],how='left')

	kbsubcol = kbtrain[['Location_id','Merchant_id','l_belongto_m','m_belongto_loc','mstartday']]
	kbsubcol = kbsubcol.drop_duplicates()
	ugbtrain = pd.merge(ugbtrain,kbsubcol,on=['Location_id','Merchant_id'],how='left')
	ugbtrain['mlrank'] = ugbtrain['histcnt']/(ugbtrain['lhistcnt']/ugbtrain['m_belongto_loc'])

	#ugbtrain.to_csv('middata/kbtrain_lsm.csv',index=False)

	return ugbtrain,kbtrain



if __name__ == '__main__':
	dist = pd.read_csv(path+'/rawdata/dist.csv')
	mrt = pd.read_csv(path+'/rawdata/ijcai2016_merchant_info.deal')
	mrt.rename(columns={'Location_id_list':'Location_id'},inplace=True)
	ugbtrain,kbtrain = split(dist,mrt,'2015-11-15','2015-11-10','2015-11-05','2015-07-01')
	#######开始预测#########
	####只取一小部分可能商家##
	ugbtrain = ugbtrain[(ugbtrain.m_belongto_loc<20)|((ugbtrain['m_belongto_loc']>=20)&(ugbtrain['mlrank']>=0.3))]
	print "ugbtrain selected shape:"+str(ugbtrain.shape)

	kbtrain = kbtrain[['User_id','Location_id','Merchant_id','ts']]
	kbtrain['ts'] = kbtrain['ts'].apply(lambda x:datetime.datetime.strptime(x,'%Y-%m-%d'))
	kbtrain = pd.merge(kbtrain,ugbtrain,on=['Location_id'],how='left')
	print kbtrain.columns,kbtrain.shape
	kbtrain = kbtrain[kbtrain['ts']>=kbtrain['mstartday']]
	kbtrain['label'] = np.where(kbtrain['Merchant_id_x']==kbtrain['Merchant_id_y'],1,0)
	del kbtrain['ts']
	kbtrain = kbtrain.dropna()
	print "kbtrain shape:"+str(kbtrain.shape)


	cvtest = dist[dist['ts']>'2015-11-15']
	cvtest = cvtest[['User_id','Location_id','Merchant_id']]
	print "cvtest shape:"+str(cvtest.shape)
	cvtest = pd.merge(cvtest,ugbtrain,on=['Location_id'],how='left')
	cvtest['label'] = np.where(cvtest['Merchant_id_x']==cvtest['Merchant_id_y'],1,0)
	cvtest = cvtest.dropna()
	print "cvtest merge shape:"+str(cvtest.shape)



	del kbtrain['Merchant_id_x']
	del cvtest['Merchant_id_x']
	kbtrain.rename(columns={'Merchant_id_y':'Merchant_id'},inplace=True)
	cvtest.rename(columns={'Merchant_id_y':'Merchant_id'},inplace=True)


	kbtrain.to_csv(path+'/middata/kbtrain.csv',index=False)
	cvtest.to_csv(path+'/middata/cvtest.csv',index=False)


	# onehots = ['User_id','Location_id','Merchant_id','mstartday']
	# target = 'label'
	# dens_feat = list(set(kbtrain.columns)^set(onehots))
	# dens_feat = list(set(dens_feat)^set([target]))

	# print "RandomForestClassifier train  begin"
	# rf = RandomForestClassifier(n_estimators=25, n_jobs = -1, random_state=1)
	# rf.fit(kbtrain[dens_feat], kbtrain[target])
	# joblib.dump(rf, path+'/model/rf.pkl')
	# print "RandomForestClassifier train end"

	# params = {"objective": "binary:logistic",
 #          "eta": 0.1,
 #          "max_depth": 5,
 #          "min_child_weight": 3,
 #          "subsample": 0.7,
 #          "colsample_bytree": 0.7,
 #          "seed": 1
	# }

	# gbm = xgb.train(params, xgb.DMatrix(kbtrain[dens_feat], kbtrain[target]), 15)
	# gbm.save_model(path+'/model/gbm0511.gbdt')

	# tr_X,tr_y,te_X,te_y = sk.sparse(kbtrain,cvtest,['User_id','Location_id','Merchant_id','mstartday'],'label')
	# param_dist = {'objective':'binary:logistic', 'n_estimators':8,'max_depth':5, 'learning_rate':1, 'silent':1}
	# clf = xgb.XGBRegressor(**param_dist)
	# clf.fit(tr_X, tr_y,eval_set=[(tr_X, tr_y), (te_X, te_y)],eval_metric='error',verbose=True)


	####做预测集######
	ugbtrain,kbtrain = split(dist,mrt,'2015-11-30','2015-11-25','2015-11-20','2015-07-15')
	ugbtrain = ugbtrain[(ugbtrain.m_belongto_loc<20)|((ugbtrain['m_belongto_loc']>=20)&(ugbtrain['mlrank']>=0.3))]
	#ugbtrain.to_csv(path+'/middata/sub_mrt.csv',index=False)
	print "sub_mrt shape:"+str(ugbtrain.shape)

	cvtest = pd.read_csv(path+'/rawdata/ijcai2016_koubei_test')
	ulmerge = pd.merge(cvtest,ugbtrain,on=['Location_id'],how='left')
	del ulmerge['Merchant_id_list']
	ulmerge = ulmerge.dropna()
	ulmerge.to_csv(path+"/middata/kbtest.csv",index=False)
	#####有交互用户单独基于统计去做####
	# ulkbtrain = dist.groupby(['User_id','Location_id']).size().reset_index()
	# ulkbtrain.rename(columns={0:'size'},inplace=True)
	# del ulkbtrain['size']
	# ulmerge = pd.merge(cvtest,ulkbtrain,on=['User_id','Location_id'],how='left')


	# cvtest_u = ulmerge[ulmerge['size'].notnull()].ix[:,['User_id','Location_id']]
	# cvtest_u = pd.merge(cvtest_u,dist,on=['User_id','Location_id'],how='inner')
	# cvtest_u = cvtest_u.groupby(['User_id','Location_id','Merchant_id']).size().reset_index()
	# cvtest_u.rename(columns={0:'cnt'},inplace=True)
	# cvtest_u_ul = cvtest_u.groupby(['User_id','Location_id']).agg({'cnt':np.sum}).reset_index()
	# cvtest_u = pd.merge(cvtest_u,cvtest_u_ul,on=['User_id','Location_id'],how='left')
	# cvtest_u['ratio'] = cvtest_u['cnt_x']/cvtest_u['cnt_y']
	# cvtest_u = cvtest_u[cvtest_u['ratio']>0.3].ix[:,['User_id','Location_id','Merchant_id']]
	# cvtest_u['preds'] = 1
	# cvtest_u.to_csv(path+'/result/cvtest_u',index=False)


	####无交互用户，通过模型训练
	# cvtest_nou = ulmerge[ulmerge['size'].isnull()].ix[:,['User_id','Location_id']]
	# cvtest_nou = pd.merge(cvtest_nou,ugbtrain,on=['Location_id'],how='left')
	# cvtest_nou['label']=0
	# cvtest_nou = cvtest_nou.dropna()
	# print "begin to predict"
	# preds = (rf.predict_proba(cvtest_nou[dens_feat])[:,1]+gbm.predict(xgb.DMatrix(cvtest_nou[dens_feat])))/2
	# print preds
	# print str(cvtest_nou.shape)
	# cvtest_nou['preds'] = preds
	# cvtest_nou.to_csv(path+'/result/cvtest_nou',index=False)
	#cvtest.to_csv('submit.csv',index=False)

	# submitgb = cvtest_nou.groupby(['User_id','Location_id']).agg({'preds':'max'}).reset_index()
	# print submitgb.shape
	# submitgb = pd.merge(submitgb,cvtest_nou,on=['User_id','Location_id','preds'],how='inner')
	# submitgb['Merchant_id'] = submitgb['Merchant_id'].fillna(820)
	# submitgb['Merchant_id'] = submitgb['Merchant_id'].apply(lambda x:int(x))
	# submitgb = submitgb[['User_id','Location_id','Merchant_id','preds']]
	# submitgb = submitgb.append(cvtest_u)

	# submitgb = pd.merge(submitgb,mrt,on=['Location_id','Merchant_id'],how='left')
	# mgb = submitgb.groupby('Merchant_id').agg({'preds':np.sum}).reset_index()
	# mgb.rename(columns={'preds':'totalpreds'},inplace=True)
	# submitgb = pd.merge(submitgb,mgb,on=['Merchant_id'],how='left')
	# submitgb['ratio'] = submitgb['totalpreds']/submitgb['Budget']
	###采样除去有交互用户
	# submitgb['rdm'] = np.where((submitgb['ratio']>1.5)&(submitgb['preds']<1),random.uniform(0, submitgb['ratio']),0)
	# submitgb.to_csv(path+'/result/submitgb',index=False)
	# submitgb = submitgb[submitgb['rdm']<=1.5]
	# submitgb = submitgb[['User_id','Location_id','Merchant_id']]


	# submitgb.to_csv(path+'/result/submit0511.csv',index=False)
