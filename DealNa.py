#!/usr/bin/python
# coding=utf-8
from __future__ import division
from math import log
import numpy as np
import pandas as pd
from math import log


class DealNa():
    def deal_na(self, df, cols):
        return

    def getEnt(self, pargs):
        sum = 0
        for i in pargs:
            sum += i * log(i, 2)
        return -sum

    def computeConEnt(self, col, tsum, df, target):
        # 求条件熵
        gb = df.groupby(by=[col, target]).size()
        gb = gb.reset_index()
        gb.rename(columns={0: 'size'}, inplace=True)
        # 列出该列所有取值
        nodelist = df[col].unique().tolist()
        sumConEnt = 0
        for i in nodelist:
            gbi = gb[gb[col] == i]
            sum = gbi['size'].sum()
            pi = sum / tsum
            gbi['cpi'] = gbi['size'] / sum
            l = [gbi['cpi']]
            pargs = np.array(l, dtype=pd.Series)
            conEnt = self.getEnt(pargs[0])
            sumConEnt = sumConEnt + conEnt * pi

        return sumConEnt


class DealNaA(DealNa):
    def deal_na(self, df, cols=None, target=None):
        if cols == None:
            df = df.dropna()
        else:
            df = df.dropna(subset=cols)
            df = df.dropna(subset=[target])

        return df


class DealNaB(DealNa):
    def deal_na(self, df, cols=None, target=None):
        len = df.shape[0]
        if target is not None:
            df = df.dropna(subset=[target])

        if cols == None:
            cols = df.columns
        for col in cols:
            count = df[col].count()
            p = count / len
            ####空值太多的列直接扔掉
            if p < 0.7:
                del df[col]
            else:
                num = df[df[col].isnull() == True].shape[0]
                if (num > 0):
                    for k, v in df[df[col].isnull() == True].iterrows():
                        a = df.sample(n=1)[col]
                        df[col].ix[k] = a.iat[0]

        return df
