#!/usr/bin/python
# coding=utf-8
from __future__ import division
import pandas as pd
import numpy as np
import datetime
import math
import os
import commands
import xgboost as xgb
import matplotlib as plt
import tensorflow as tf
pd.set_option('display.max_columns', 80)
pd.set_option('display.max_rows', 200)
pd.set_option('expand_frame_repr', True)

def get_5_10_td(train):
	train['10min'] = train['minute'].apply(lambda x:math.floor(((x-0.000001)/10))+1)
	train['5min'] = train['minute'].apply(lambda x:math.floor(((x-0.000001)/5))+1)
	fdm_gb5 = train.groupby(['fromid','date','weekend','5min']).agg({'cnt':np.sum,'success':np.sum}).reset_index()
	fdm_gb10 = train.groupby(['fromid','date','weekend','10min']).agg({'cnt':np.sum,'success':np.sum}).reset_index()
	fdm_gb5['gap'] = fdm_gb5['cnt']-fdm_gb5['success']
	fdm_gb10['gap'] = fdm_gb10['cnt']-fdm_gb10['success']
	return fdm_gb5,fdm_gb10

def gbdt_train(dftrain,dftest,dense_feature,target,param,num_round):

	watchlist  = [(xgb.DMatrix(dftest[dense_feature], dftest[target]),'eval'),
	(xgb.DMatrix(dftrain[dense_feature], dftrain[target]),'train')]

	bst = xgb.train(param, xgb.DMatrix(dftrain[dense_feature], dftrain[target]), num_round, watchlist)
	return bst

def train_gbdt_model(dtrain,dval,dense_feature,target,param,num_round_tree):
	model = gbdt_train(dtrain,dval,dense_feature,target,param,num_round_tree)
	dval['preds'] = model.predict(xgb.DMatrix(dval[dense_feature]))
	dval['preds'] = np.where(dval['preds']<1,1,dval['preds'])
	dval['mape'] = np.where(dval['gapaf1']>0,abs(dval['preds']-dval['gapaf1'])/dval['gapaf1'],0)
	print dval[dval['mape']>0].ix[:,'mape'].describe()
	return model,dval

def gen_gap_feat(df):
	#cnt = np.array(df['cnt'])
	success = np.array(df['gap'])
	fromid = np.array(df['fromid'])
	larray = len(success)
	sucbf1 = np.zeros(larray)
	sucbf2 = np.zeros(larray)
	sucaf1 = np.zeros(larray)
	sucaf2 = np.zeros(larray)

	for i in range(0,larray):
		if i-1>=0:
			if fromid[i]==fromid[i-1]:
				sucbf1[i] = success[i-1]
			else:
				sucbf1[i] = success[i]
		else:
			sucbf1[i] = success[i]

		if i-2>=0:
			if fromid[i]==fromid[i-2]:
				sucbf2[i] = success[i-2]
			else:
				sucbf2[i] = success[i]
		else:
			sucbf2[i] = success[i]

		if i+1<larray:
			if fromid[i]==fromid[i+1]:
				sucaf1[i] = success[i+1]
			else:
				sucaf1[i] = success[i]
		else:
			sucaf1[i] = success[i]

		if i+2<larray:
			if fromid[i]==fromid[i+1]:
				sucaf2[i] = success[i+2]
			else:
				sucaf2[i] = success[i]
		else:
			sucaf1[i] = success[i]
	df['gapbf1'] = sucbf1
	df['gapbf2'] = sucbf2
	df['gapaf1'] = sucaf1
	df['gapaf2'] = sucaf2

	df['gapba30'] = (df['gap']+df['gapaf1']+df['gapaf2'])/3
	df['gapb20'] = (df['gapbf1']+df['gapbf2'])/2
	df['gapratio'] = df['gapaf1']/(df['gap']+1)
	df['gapbratio'] = df['gap']/(df['gapbf1']+1)
	df['gaparatio'] = df['gapaf2']/(df['gapaf1']+1)
	return df

def gen_req_feat(df):
	cnt = np.array(df['cnt'])
	#success = np.array(df['success'])
	fromid = np.array(df['fromid'])
	larray = len(cnt)
	cntbf1 = np.zeros(larray)
	cntbf2 = np.zeros(larray)
	cntaf1 = np.zeros(larray)
	cntaf2 = np.zeros(larray)
	# sucbf1 = np.zeros(larray)
	# sucbf2 = np.zeros(larray)
	# sucaf1 = np.zeros(larray)

	for i in range(0,larray):
		if i-1>=0:
			if fromid[i]==fromid[i-1]:
				cntbf1[i] = cnt[i-1]
				#sucbf1[i] = success[i-1]
			else:
				cntbf1[i] = cnt[i]
				#sucbf1[i] = success[i]
		else:
			cntbf1[i] = cnt[i]
			#sucbf1[i] = success[i]

		if i-2>=0:
			if fromid[i]==fromid[i-2]:
				cntbf2[i] = cnt[i-2]
				#sucbf2[i] = success[i-2]
			else:
				cntbf2[i] = cnt[i]
				#sucbf2[i] = success[i]
		else:
			cntbf2[i] = cnt[i]
			#sucbf2[i] = success[i]

		if i+1<larray:
			if fromid[i]==fromid[i+1]:
				cntaf1[i] = cnt[i+1]
				#sucaf1[i] = success[i+1]
			else:
				cntaf1[i] = cnt[i]
				#sucaf1[i] = success[i]
		else:
			cntaf1[i] = cnt[i]

		if i+2<larray:
			if fromid[i]==fromid[i+1]:
				cntaf2[i] = cnt[i+2]
				#sucaf1[i] = success[i+1]
			else:
				cntaf2[i] = cnt[i]
				#sucaf1[i] = success[i]
		else:
			cntaf2[i] = cnt[i]
			#sucaf1[i] = success[i]
	df['cntbf1'] = cntbf1
	df['cntbf2'] = cntbf2
	df['cntaf1'] = cntaf1
	df['cntaf2'] = cntaf2

	df['ba30cnt'] = (df['cnt']+df['cntaf1']+df['cntaf2'])/3
	df['b20cnt'] = (df['cntbf1']+df['cntbf2'])/2
	df['cntratio'] = df['cntaf1']/(df['cnt']+1)
	df['cntbratio'] = df['cnt']/(df['cntbf1']+1)
	df['cntaratio'] = df['cntaf2']/(df['cntaf1']+1)
	return df

def get_all_dtime(torder):
	torder['weekday'] = torder['date'].apply(lambda x:datetime.datetime.strptime(x,'%Y-%m-%d').weekday())
	torder['weekday'] = torder['weekday']+1
	torder['weekend'] = 0
	torder['weekend'] = np.where(torder['weekday']==6,2,torder['weekend'])
	torder['weekend'] = np.where(torder['weekday']==7,1,torder['weekend'])
	torder['weekend'] = np.where(torder['weekday']==1,3,torder['weekend'])
	del torder['weekday']
	return torder

def get_train_reqtmp_feat(train10req):
	reqfeat = train10req.groupby(['fromid','weekend','10min'])['cntaf2','cntaf1','cnt','b20cnt','ba30cnt','cntratio','cntbratio','cntaratio'].agg({'mean':np.mean,'std':np.std}).reset_index()
	reqfeat.columns = [' '.join(col).strip() for col in reqfeat.columns.values]
	reqfeatpart = reqfeat[['fromid','weekend','10min','mean cntaf2','mean cntaf1','mean cnt','mean b20cnt','mean ba30cnt','mean cntratio','mean cntbratio','mean cntaratio','std cntaf2','std cntaf1','std cnt','std b20cnt','std ba30cnt']]
	return reqfeatpart
def get_merge_req_feat(train10_merge):
	train10_merge['b10cntnor'] = (train10_merge['cnt']-train10_merge['mean cnt'])/(train10_merge['std cnt']+0.00001)
	train10_merge['b20cntnor'] = (train10_merge['b20cnt']-train10_merge['mean b20cnt'])/(train10_merge['std b20cnt']+0.00001)
	train10_merge['cntrationor'] = train10_merge['cntbratio']/(train10_merge['mean cntbratio']+0.00001)
	train10_merge['pcnt'] = train10_merge['cnt']*train10_merge['mean cntratio']
	#train10_merge = train10_merge[(train10_merge['10min']>2)&(train10_merge['10min']<145)]
	return train10_merge

def merge_req_feat(train10req):
	reqfeat = train10req.groupby(['fromid','weekend','10min'])['cntaf2','cntaf1','cnt','b20cnt','ba30cnt','cntratio','cntbratio','cntaratio'].agg({'mean':np.mean,'std':np.std}).reset_index()
	reqfeat.columns = [' '.join(col).strip() for col in reqfeat.columns.values]
	reqfeatpart = reqfeat[['fromid','weekend','10min','mean cntaf2','mean cntaf1','mean cnt','mean b20cnt','mean ba30cnt','mean cntratio','mean cntbratio','mean cntaratio','std cntaf2','std cntaf1','std cnt','std b20cnt','std ba30cnt']]
	train10_merge = pd.merge(train10req,reqfeatpart,on=['fromid','weekend','10min'],how='left')

	train10_merge['b10cntnor'] = (train10_merge['cnt']-train10_merge['mean cnt'])/(train10_merge['std cnt']+0.00001)
	train10_merge['b20cntnor'] = (train10_merge['b20cnt']-train10_merge['mean b20cnt'])/(train10_merge['std b20cnt']+0.00001)
	train10_merge['cntrationor'] = train10_merge['cntbratio']/(train10_merge['mean cntbratio']+0.00001)
	train10_merge['pcnt'] = train10_merge['cnt']*train10_merge['mean cntratio']
	#train10_merge = train10_merge[(train10_merge['10min']>2)&(train10_merge['10min']<145)]
	return train10_merge


def get_train_gaptmp_feat(train10gap):
	gapfeat = train10gap.groupby(['fromid','weekend','10min'])['success','gapaf2','gapaf1','gap','gapb20','gapba30','gapratio','gaparatio'].agg({'mean':np.mean,'std':np.std}).reset_index()
	gapfeat.columns = [' '.join(col).strip() for col in gapfeat.columns.values]
	gapfeatpart = gapfeat[['fromid','weekend','10min','mean success','std success','mean gapaf2','std gapaf2','mean gapaf1','std gapaf1','mean gap','std gap','mean gapb20','std gapb20','mean gapba30','std gapba30','mean gapratio','mean gaparatio']]
	return gapfeatpart
def get_merge_gap_feat(train10gap_merge):
	train10gap_merge['gapnor'] = (train10gap_merge['gap']-train10gap_merge['mean gap'])/(train10gap_merge['std gap']+0.00001)
	train10gap_merge['gapb20nor'] = (train10gap_merge['gapb20']-train10gap_merge['mean gapb20'])/(train10gap_merge['std gapb20']+0.00001)
	#train10gap_merge['gaprationor'] = train10gap_merge['gapratio']/(train10gap_merge['mean gapratio']+0.0000001)
	train10gap_merge['pgap'] = train10gap_merge['gap']*train10gap_merge['mean gapratio']
	#train10gap_merge = train10gap_merge[(train10_merge['10min']>2)&(train10gap_merge['10min']<145)]
	return train10gap_merge


def merge_gap_feat(train10gap):
	gapfeat = train10gap.groupby(['fromid','weekend','10min'])['success','gapaf2','gapaf1','gap','gapb20','gapba30','gapratio','gaparatio'].agg({'mean':np.mean,'std':np.std}).reset_index()
	gapfeat.columns = [' '.join(col).strip() for col in gapfeat.columns.values]
	gapfeatpart = gapfeat[['fromid','weekend','10min','mean success','std success','mean gapaf2','std gapaf2','mean gapaf1','std gapaf1','mean gap','std gap','mean gapb20','std gapb20','mean gapba30','std gapba30','mean gapratio','mean gaparatio']]

	train10gap_merge = pd.merge(train10gap,gapfeatpart,on=['fromid','weekend','10min'],how='left')

	train10gap_merge['gapnor'] = (train10gap_merge['gap']-train10gap_merge['mean gap'])/(train10gap_merge['std gap']+0.00001)
	train10gap_merge['gapb20nor'] = (train10gap_merge['gapb20']-train10gap_merge['mean gapb20'])/(train10gap_merge['std gapb20']+0.00001)
	#train10gap_merge['gaprationor'] = train10gap_merge['gapratio']/(train10gap_merge['mean gapratio']+0.0000001)
	train10gap_merge['pgap'] = train10gap_merge['gap']*train10gap_merge['mean gapratio']
	#train10gap_merge = train10gap_merge[(train10_merge['10min']>2)&(train10gap_merge['10min']<145)]
	return train10gap_merge


if __name__ == '__main__':
	path = "/Users/simon/Downloads/didi/middata/"
	order = pd.read_csv(path+'ordermid.csv')
	tidx10 = pd.read_csv(path+'tidx10.csv')
	tidx5 = pd.read_csv(path+'tidx5.csv')

	train = order[order.delta<19]
	test = order[order.delta>18]
	order['10min'] = order['minute'].apply(lambda x:math.floor((x/10))+1)
	order['5min'] = order['minute'].apply(lambda x:math.floor((x/5))+1)
	fdm_gb5,fdm_gb10 = get_5_10_td(train)
	train10 = pd.merge(tidx10,fdm_gb10,on=['fromid','date','10min'],how='left')
	##########处理训练集##############
	###生成weekend##
	train10 = get_all_dtime(train10)
	train10 = train10.fillna(0)
	###########做req的feature#############
	train10req = gen_req_feat(train10)
	train10req_merge = merge_req_feat(train10req)
	req_feature = ['weekend','cntbf1','cntbf2','b20cnt','mean cntaf1','mean cnt','mean cntbf1','std cntbf1','mean b20cnt','mean ba30cnt','std cnt','b10cntnor','b20cntnor','cntbrationor','mean cntratio','mean cntbratio','mean cntaratio','pcnt']

	#################END REQ FEAT#######################


	###########做suc的feature#############
	train10suc = gen_suc_feat(train10)
	train10suc_merge = merge_suc_feat(train10suc)
	suc_feature = ['weekend','cntbf1','sucbf1','sucbf2','b20suc','sucbratio','mean cnt','std cnt','mean cntbf1','mean sucaf1','std sucaf1','smax sucaf1','mean success','std success','smax success','mean sucbf1','std sucbf1','smax sucbf1','mean b20suc','mean ba30suc','mean sucratio','mean sucbratio','mean sucaratio','b10sucnor','b20sucnor','sucbrationor','psuc']

	##################END SUC FEAT###########################

	############hehe########
	alldist = merge1.fromid.unique()
	bigdist = [23,7,8,1,51,48]
	middist = [37,46,28,12,14,24,42,27,20,21,19,4]
	mindist = list(set(alldist)^(set(bigdist)|set(middist)))
	def split_dist(merge1):
		merge1big = merge1[(merge1.fromid.isin(bigdist))]
		merge1mid = merge1[(merge1.fromid.isin(middist))]
		merge1min = merge1[(merge1.fromid.isin(mindist))]
		return merge1big,merge1mid,merge1min

	###########################

	##################训练req######################

	param_tree = {"objective": "reg:linear",
	          "eta": 0.1,
	          "max_depth": 8,
	          "min_child_weight": 3,
	          "subsample":0.8,
	          "colsample_bytree":0.9,
	          "booster":"gbtree",
	          "eval_metric":"rmse"
		}

	param_liner = {"objective": "reg:linear",
		"eta": 0.15,
		"booster ":"gblinear",
		"lambda":0.1,
		"eval_metric":"rmse",
	}

	reqtree = train_gbdt_model(train10req_merge,train10req_merge,dense_feature,'cnt',param_tree,50)
	reqlinear = train_gbdt_model(train10req_merge,train10req_merge,dense_feature,'cnt',param_liner,50)


	#####################################################


	####################训练suc###########################

	param_tree = {"objective": "reg:linear",
	          "eta": 0.1,
	          "max_depth": 8,
	          "min_child_weight": 3,
	          "subsample":0.8,
	          "colsample_bytree":0.9,
	          "booster":"gbtree",
	          "eval_metric":"rmse"
		}

	param_liner = {"objective": "reg:linear",
		"eta": 0.15,
		"booster ":"gblinear",
		"lambda":0.1,
		"eval_metric":"rmse",
	}

	suctree = train_gbdt_model(train10suc_merge,train10suc_merge,suc_dense_feature,'success',param_tree,50)
	suclinear = train_gbdt_model(train10suc_merge,train10suc_merge,suc_dense_feature,'success',param_liner,50)

	######################################################

