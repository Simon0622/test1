from __future__ import division
import rundidi as dd
import pandas as pd
import numpy as np
import datetime
import math
import os
import commands
import xgboost as xgb
import matplotlib as plt

path = "/Users/simon/Downloads/didi/middata/"
order = pd.read_csv(path+'ordermid.csv')
tidx10 = pd.read_csv(path+'tix10.csv')
#tidx5 = pd.read_csv(path+'tidx5.csv')

train = order[order.delta<19]
test = order[order.delta>18]
fdm_gb5,fdm_gb10 = dd.get_5_10_td(train)
#####补齐缺失时间片
train10 = pd.merge(tidx10,fdm_gb10,on=['fromid','date','10min'],how='left')
##########处理训练集##############
###生成weekend##
train10 = dd.get_all_dtime(train10)
train10 = train10.fillna(0)
###########做req的feature#############
train10req = dd.gen_req_feat(train10)
train10reqm = dd.merge_req_feat(train10req)
traintmpreq = dd.get_train_reqtmp_feat(train10req)

#################END REQ FEAT#######################


###########做suc的feature#############
train10gap = dd.gen_gap_feat(train10reqm)
train10gapm = dd.merge_gap_feat(train10gap)
traintmpgap = dd.get_train_gaptmp_feat(train10gap)
defeat = ['date','10min','fromid','cntaf1','cntaf2','ba30cnt','cntratio','cntaratio','gapaf1','gapaf2','gapba30','gaparatio','gapratio']
dfeature = list(set(train10gapm)^set(defeat))
train10gapm = train10gapm[(train10gapm['10min']>2)&(train10gapm['10min']<143)]
# suc_feature = ['weekend','cntbf1','sucbf1','sucbf2','b20suc','sucbratio','mean cnt','std cnt','mean cntbf1','mean sucaf1','std sucaf1','smax sucaf1','mean success','std success','smax success','mean sucbf1','std sucbf1','smax sucbf1','mean b20suc','mean ba30suc','mean sucratio','mean sucbratio','mean sucaratio','b10sucnor','b20sucnor','sucbrationor','psuc']

##################END SUC FEAT###########################


############生成测试集#################
testgb5,testgb10 = dd.get_5_10_td(test)
testgb10 = dd.get_all_dtime(testgb10)

test10req = dd.gen_req_feat(testgb10)
test10reqm = pd.merge(test10req,traintmpreq,on=['fromid','weekend','10min'],how='left')
test10reqm = dd.get_merge_req_feat(test10reqm)


test10gap = dd.gen_gap_feat(test10reqm)
test10gapm = pd.merge(test10gap,traintmpgap,on=['fromid','weekend','10min'],how='left')
test10gapm = dd.get_merge_gap_feat(test10gapm)
test10gapm = test10gapm[(test10gapm['10min']>2)&(test10gapm['10min']<143)]
############hehe########
alldist = order.fromid.unique()
bigdist = [23,7,8,1,51,48]
middist = [37,46,28,12,14,24,42,27,20,21,19,4]
mindist = list(set(alldist)^(set(bigdist)|set(middist)))
def split_dist(merge1):
	merge1big = merge1[(merge1.fromid.isin(bigdist))]
	merge1mid = merge1[(merge1.fromid.isin(middist))]
	merge1min = merge1[(merge1.fromid.isin(mindist))]
	return merge1big,merge1mid,merge1min

trbig,trmid,trmin = split_dist(train10gapm)
tebig,temid,temin = split_dist(test10gapm)
###########################

##################训练req######################

param_tree = {"objective": "reg:linear",
          "eta": 0.1,
          "max_depth": 5,
          "min_child_weight": 3,
          "subsample":0.8,
          "colsample_bytree":0.8,
          "booster":"gbtree",
          "eval_metric":"rmse"
	}

param_liner = {"objective": "reg:linear",
	"eta": 0.2,
	"booster ":"gblinear",
	"lambda":0.1,
	"eval_metric":"rmse",
}

reqtreebig,_ = dd.train_gbdt_model(trbig,tebig,dfeature,'gapaf1',param_tree,90)
reqtreemid,_ = dd.train_gbdt_model(trmid,temid,dfeature,'gapaf1',param_tree,50)
reqtreemin,_ = dd.train_gbdt_model(trmin,temin,dfeature,'gapaf1',param_tree,50)
reqlinearbig,_ = train_gbdt_model(trbig,tebig,dfeature,'gapaf1',param_liner,90)
reqlinearmid,_ = dd.train_gbdt_model(trmid,temid,dfeature,'gapaf1',param_liner,50)
reqlinearmin,_ = train_gbdt_model(trmin,temin,dfeature,'gapaf1',param_liner,50)

test10gapm['predl'] = reqtree.predict(xgb.DMatrix(test10gapm[dfeature]))





lastbig,lastmid,lastmin = split_dist(last)
lastbig['preds'] = reqtreebig.predict(xgb.DMatrix(lastbig[dfeature]))
lastmid['preds'] = reqtreemid.predict(xgb.DMatrix(lastmid[dfeature]))
lastmin['preds'] = reqtreemin.predict(xgb.DMatrix(lastmin[dfeature]))
lastbig[['date','10min','fromid','preds']].to_csv('lastb.csv',index=False)
lastmid[['date','10min','fromid','preds']].to_csv('lastmid.csv',index=False)
lastmin[['date','10min','fromid','preds']].to_csv('lastmin.csv',index=False)


def do_some_trick():
	temin['preds'] = temin['mean gapba30']
	temin['preds'] = np.where(temin['preds']<1,1,temin['preds'])
	temin['mape'] = np.where(temin['gapaf1']>0,abs(temin['preds']-temin['gapaf1'])/temin['gapaf1'],0)
	temin[temin['mape']>0].ix[:,'mape'].describe()


import tensorflow as tf
from tensorflow.python.framework import dtypes
from tensorflow.contrib import learn
from sklearn.metrics import mean_squared_error
import logging

logging.basicConfig(level=logging.INFO)
LOG_DIR = './ops_logs'
TIMESTEPS = 3
RNN_LAYERS = [{'steps': TIMESTEPS,'keep_prob':0.9}]
DENSE_LAYERS = None
TRAINING_STEPS = 500000
BATCH_SIZE = 20
PRINT_STEPS = TRAINING_STEPS / 500

def gen_rnn_feat(rtrain,feature,target):
	trcntx = rtrain[feature].values
	trcnty = np.array(rtrain[target])

	trainx = []
	trainy = []
	for i in range(0,len(trcnty)-TIMESTEPS):
		tmpx = []
		for j in range(0,TIMESTEPS):
			tmpx.append(trcntx[i+j])
		trainx.append(tmpx)
		trainy.append(trcnty[i])

	trainx = np.array(trainx)
	trainy = np.array(trainy)
	return trainx,trainy

trmin = trmin[trmin['gapaf1']>0]
temin = temin[temin['gapaf1']>0]
trainx,trainy = gen_rnn_feat(trmin,dfeature,'gapaf1')
testx,testy = gen_rnn_feat(temin,dfeature,'gapaf1')
regressor = learn.TensorFlowEstimator(model_fn=lstm_model(TIMESTEPS, RNN_LAYERS, [3,1]), n_classes=0,verbose=2,steps=TRAINING_STEPS,optimizer='Adagrad',learning_rate=0.5,batch_size=BATCH_SIZE)
validation_monitor = learn.monitors.ValidationMonitor(testx, testy,print_steps=PRINT_STEPS)
regressor.fit(trainx, trainy, monitor=validation_monitor, logdir=LOG_DIR)
