import tensorflow as tf
from tensorflow.models.rnn import rnn, rnn_cell
import numpy as np
import pandas as pd

# Parameters
learning_rate = 0.01
training_iters = 100000
batch_size = 15
display_step = 20

# Network Parameters
n_input = 4 # MNIST data input (img shape: 28*28)
n_steps = 20 # timesteps
n_hidden = 10 # hidden layer num of features
n_classes = 1

# tf Graph input
x = tf.placeholder("float64", [None, n_steps, n_input])
# Tensorflow LSTM cell requires 2x n_hidden length (state & cell)
istate = tf.placeholder("float64", [None, 2*n_hidden])
y = tf.placeholder("float64", [None])

# Define weights
weights = {
    'hidden': tf.Variable(tf.random_normal([n_input, n_hidden])), # Hidden layer weights
    'out': tf.Variable(tf.random_normal([n_hidden, n_classes]))
}
biases = {
    'hidden': tf.Variable(tf.random_normal([n_hidden])),
    'out': tf.Variable(tf.random_normal([n_classes]))
}

def gen_cosine_amp(amp=100, period=25, x0=0, xn=50000, step=1, k=0.0001):
    """Generates an absolute cosine time series with the amplitude
    exponentially decreasing
    Arguments:
        amp: amplitude of the cosine function
        period: period of the cosine function
        x0: initial x of the time series
        xn: final x of the time series
        step: step of the time series discretization
        k: exponential rate
    """
    cos = np.zeros(((xn - x0) * step))
    for i in range(len(cos)):
        idx = x0 + i * step
        cos[i] = amp * np.cos(idx / (2 * np.pi * period))
        cos[i] = cos[i] * np.exp(-k * idx)
    return cos

def rnn_data(data, time_steps, labels=False):
    """
    creates new data frame based on previous observation
      * example:
        l = [1, 2, 3, 4, 5]
        time_steps = 2
        -> labels == False [[1, 2], [2, 3], [3, 4]]
        -> labels == True [2, 3, 4, 5]
    """
    if not isinstance(data, pd.DataFrame):
        data = pd.DataFrame(data)
    rnn_df = []
    for i in range(len(data) - time_steps - 10):
        if labels:
            try:
                rnn_df.append(data.iloc[i + time_steps + 10].as_matrix())
            except AttributeError:
                rnn_df.append(data.iloc[i + time_steps])
        else:
            data_ = data.iloc[i: i + time_steps].as_matrix()
            rnn_df.append(data_ if len(data_.shape) > 1 else [[i] for i in data_])
    return np.array(rnn_df)


def RNN(_X, _istate, _weights, _biases):

    
    _X = tf.transpose(_X, [1, 0, 2])  # permute n_steps and batch_size
    
    _X = tf.reshape(_X, [-1, n_input]) # (n_steps*batch_size, n_input)
   
    _X = tf.matmul(_X, _weights['hidden']) + _biases['hidden']

   
    lstm_cell = rnn_cell.BasicLSTMCell(n_hidden, forget_bias=1.0)
    
    _X = tf.split(0, n_steps, _X) # n_steps * (batch_size, n_hidden)

   
    outputs, states = rnn.rnn(lstm_cell, _X, initial_state=_istate)

    # Get inner loop last output
    return tf.matmul(outputs[-1], _weights['out']) + _biases['out']

pred = RNN(x, istate, weights, biases)

cost = tf.reduce_sum((y-tf.reduce_mean(pred,1))**2)
# Define loss and optimizer
#cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(pred, y)) # Softmax loss
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost) # Adam Optimizer

# Evaluate model
# correct_pred = tf.equal(tf.argmax(pred,1), tf.argmax(y,1))
# accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

# Initializing the variables
init = tf.initialize_all_variables()
sess = tf.InteractiveSession()
sess.run(init)
step = 1
# xx = []
# for i in range(0,50000):
#     xx.append(i)

# xx = np.array(xx)
# xx = xx.astype('float')
# yy = gen_cosine_amp(x0=0, xn=50000)
# x1 = rnn_data(xx,n_steps)
# y1 = rnn_data(yy,n_steps,labels=True)

while step  < 1000:
    batch_xs = cntx[(step-1) * batch_size:step * batch_size]
    batch_ys = cnty[(step-1) * batch_size:step * batch_size]
    batch_ys = tf.reduce_mean(batch_ys,1)
    batch_ys = batch_ys.eval()
    #batch_xs = batch_xs.reshape((batch_size, n_steps, n_input))
    sess.run(optimizer, feed_dict={x: batch_xs, y: batch_ys,istate: np.zeros((batch_size, 2*n_hidden))})
    if step % display_step == 0:
        loss = sess.run(cost, feed_dict={x: batch_xs, y: batch_ys, istate: np.zeros((batch_size, 2*n_hidden))})
        print "Iter " + str(step*batch_size) + ", Minibatch Loss= " + "{:.6f}".format(loss)
    step = step + 1
print "Optimization Finished!"