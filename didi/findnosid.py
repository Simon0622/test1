#!/usr/bin/python
# coding=utf-8
from __future__ import division
import pandas as pd
import numpy as np
import datetime
import math
import os
import commands
import math

#path = "/Users/simon/Downloads/didi/training_data/"
path = "/dd/season_1/training_data/"
order = pd.read_csv(path+'order_all.csv')
del nosid['oid']
cluster = pd.read_csv(path+'Custer_map.csv')
nosid = order[order.sid.isnull()]

nosid = pd.merge(nosid,cluster,left_on=['fromhash'],right_on=['dshash'],how='left')
del nosid['dshash']
nosid.rename(columns={'dsid':'fromid'},inplace=True)

nosid = pd.merge(nosid,cluster,left_on='tohash',right_on='dshash',how='left')
del nosid['dshash']
nosid.rename(columns={'dsid':'toid'},inplace=True)
nosid['hh'] = nosid['disttime'].apply(lambda x:math.floor(x/3)+1)

