#!/usr/bin/python
# coding=utf-8
from __future__ import division
import pandas as pd
import numpy as np
import datetime
import math
import os
import commands
import math

def jdratio_single(df,cols,target):
	for col in cols:
		gb = df.groupby([col,target]).size().reset_index()
		gb_pvt = gb.pivot_table(0,[col],target).reset_index()
		tgb_pvt[col+'ratio'] = tgb_pvt[0]/(tgb_pvt[0]+tgb_pvt[1])

def price_dict(price):
	if price<=7:
		return 1
	elif price>7 and price<=8.4:
		return 2
	elif price>8.4 and price<=11:
		return 3
	elif price>11 and price<=15:
		return 4
	elif price>15 and price<=20:
		return 5
	elif price>20 and price<=28:
		return 6
	else:
		return 7

pd.set_option('display.max_columns', 80)
pd.set_option('display.max_rows', 200)
pd.set_option('expand_frame_repr', False)
path = "/dd/season_1/training_data/"
order = pd.read_csv(path+'order_data/order_all.csv')
del order['oid']
del order['uid']
traffic = pd.read_csv(path+'traffic_data/traffic_all.csv')
weather = pd.read_csv(path+'weather_data/weather_all.csv')
cluster = pd.read_table(path+'cluster_map/cluster_map')
poi = pd.read_csv(path+'poi_data/poi_pvt.csv')
poi = poi.fillna(0)

order['half_h'] = order['disttime'].apply(lambda x:math.floor(x/3)+1)
traffic['half_h'] = traffic['disttime'].apply(lambda x:math.floor(x/3)+1)

order = pd.merge(order,cluster,left_on='fromhash',right_on='dhs',how='left')
del order['fromhash']
del order['dhs']
order = pd.merge(order,cluster,left_on='tohash',right_on='dhs',how='left')
del order['tohash']
del order['dhs']
order.rename(columns={'did_x':'fromdid','did_y':'todid'},inplace=True)