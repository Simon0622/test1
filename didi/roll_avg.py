#!/usr/bin/python
# coding=utf-8
#%matplotlib osx
from __future__ import division
import pandas as pd
import numpy as np
import datetime
import math
import os
import commands
import xgboost as xgb
import matplotlib as plt
import tensorflow as tf
from tensorflow.models.rnn import rnn, rnn_cell

path = "/Users/simon/Downloads/didi/middata/"
order = pd.read_csv(path+'ordermid.csv')

mk = order.groupby(['ljminute','fromid']).agg({'success':np.sum,'cnt':np.sum,'minute':np.mean}).reset_index()
mk = mk.sort_values('ljminute')


####随便赋的，因为开始几个并不重要
last1 = 0
last2 = 0
last3 = 0
last4 = 0
last5 = 0
minu = []
min5 = []
min10 = []
min20 = []
min30 = []
x = []
y = []
suc = []

queue = np.zeros(30)
k = 1
for index,row in mk[(mk.fromid==51)].iterrows():
	if k<30:
		k = k+1
	else:
		x.append(row['ljminute'])
		y.append(row['cnt'])
		for i in range(len(queue)-1,0,-1):
			queue[i] = queue[i-1]
		queue[0] = row['cnt']

		minu.append(row['minute'])
		min5.append(queue[:5].sum()/5)
		min10.append(queue[:10].sum()/10)
		min20.append(queue[:20].sum()/20)
		min30.append(queue.sum()/30)
	#suc.append(row['success'])
	# min10.append((row['cnt']+last1)/2)
	# min20.append((row['cnt']+last1+last2+last3)/4)
	# min30.append((row['cnt']+last1+last2+last3+last4+last5)/6)


	# last5 = last4
	# last4 = last3
	# last3 = last2
	# last2 = last1
	# last1 = row['cnt']

# plt.figure(3)
# #plt.plot(x, y,label='min1')
# plt.plot(x, min5,label='min5')
# plt.plot(x, min10,label='min10')
# plt.plot(x, min20,label='min20')
# plt.plot(x, min30,label='min30')
# plt.legend(loc='upper left')
# plt.plot(x, suc)
# print minu[0:1],min5[0:1],min10[0:1],min20[0:1],min30[0:1]
learning_rate = 0.01
training_iters = 100000
batch_size = 15
display_step = 20

# Network Parameters
n_input = 1 # MNIST data input (img shape: 28*28)
n_steps = 20 # timesteps
n_hidden = 10 # hidden layer num of features
n_classes = 1

# tf Graph input
x = tf.placeholder("float32", [None, n_steps, n_input])
# Tensorflow LSTM cell requires 2x n_hidden length (state & cell)
istate = tf.placeholder("float32", [None, 2*n_hidden])
y = tf.placeholder("float32", [None,n_input])

# Define weights
weights = {
    'hidden': tf.Variable(tf.random_normal([n_input, n_hidden])), # Hidden layer weights
    'out': tf.Variable(tf.random_normal([n_hidden, n_classes]))
}
biases = {
    'hidden': tf.Variable(tf.random_normal([n_hidden])),
    'out': tf.Variable(tf.random_normal([n_classes]))
}

def gen_cosine_amp(amp=100, period=25, x0=0, xn=50000, step=1, k=0.0001):
    """Generates an absolute cosine time series with the amplitude
    exponentially decreasing
    Arguments:
        amp: amplitude of the cosine function
        period: period of the cosine function
        x0: initial x of the time series
        xn: final x of the time series
        step: step of the time series discretization
        k: exponential rate
    """
    cos = np.zeros(((xn - x0) * step))
    for i in range(len(cos)):
        idx = x0 + i * step
        cos[i] = amp * np.cos(idx / (2 * np.pi * period))
        cos[i] = cos[i] * np.exp(-k * idx)
    return cos

def rnn_data(data, time_steps, labels=False):
    """
    creates new data frame based on previous observation
      * example:
        l = [1, 2, 3, 4, 5]
        time_steps = 2
        -> labels == False [[1, 2], [2, 3], [3, 4]]
        -> labels == True [2, 3, 4, 5]
    """
    rnn_df = []
    for i in range(len(data) - time_steps - 1):
        if labels:
            try:
                rnn_df.append(data.iloc[i + time_steps + 1].as_matrix())
            except AttributeError:
                rnn_df.append(data.iloc[i + time_steps])
        else:
            data_ = data.iloc[i: i + time_steps].as_matrix()
            rnn_df.append(data_ if len(data_.shape) > 1 else [[i] for i in data_])
    return np.array(rnn_df)


def split_data(data, val_size=0.1, test_size=0.1):
    """
    splits data to training, validation and testing parts
    """
    ntest = int(round(len(data) * (1 - test_size)))
    nval = int(round(len(data.iloc[:ntest]) * (1 - val_size)))

    df_train, df_val, df_test = data.iloc[:nval], data.iloc[nval:ntest], data.iloc[ntest:]

    return df_train, df_val, df_test


def prepare_data(data, time_steps, labels=False, val_size=0.1, test_size=0.1):
    """
    Given the number of `time_steps` and some data,
    prepares training, validation and test data for an lstm cell.
    """
    df_train, df_val, df_test = split_data(data, val_size, test_size)
    train = rnn_data(df_train, time_steps, labels=labels)
    val = rnn_data(df_val, time_steps, labels=labels)
    test = rnn_data(df_test, time_steps, labels=labels)
    return (train,val,test)


def generate_data(fct, x, time_steps, seperate=False):
    """generates data with based on a function fct"""
    data = fct(x)
    if not isinstance(data, pd.DataFrame):
        data = pd.DataFrame(data)
    train_x, val_x, test_x = prepare_data(data['a'] if seperate else data, time_steps)
    train_y, val_y, test_y = prepare_data(data['b'] if seperate else data, time_steps, labels=True)
    return dict(train=train_x, val=val_x, test=test_x), dict(train=train_y, val=val_y, test=test_y)

def RNN(_X, _istate, _weights, _biases):

    
    _X = tf.transpose(_X, [1, 0, 2])  # permute n_steps and batch_size
    
    _X = tf.reshape(_X, [-1, n_input]) # (n_steps*batch_size, n_input)
   
    _X = tf.matmul(_X, _weights['hidden']) + _biases['hidden']

   
    lstm_cell = rnn_cell.BasicLSTMCell(n_hidden, forget_bias=1.0)
    
    _X = tf.split(0, n_steps, _X) # n_steps * (batch_size, n_hidden)

   
    outputs, states = rnn.rnn(lstm_cell, _X, initial_state=_istate)

    # Get inner loop last output
    return tf.matmul(outputs[-1], _weights['out']) + _biases['out']

pred = RNN(x, istate, weights, biases)

cost = tf.reduce_sum((tf.reduce_mean(y,1)-tf.reduce_mean(pred,1))**2)
# Define loss and optimizer
#cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(pred, y)) # Softmax loss
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost) # Adam Optimizer

# Evaluate model
# correct_pred = tf.equal(tf.argmax(pred,1), tf.argmax(y,1))
# accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

# Initializing the variables
init = tf.initialize_all_variables()
sess = tf.InteractiveSession()
sess.run(init)

# xx = []
# for i in range(0,50000):
#     xx.append(i)

# xx = np.array(xx)
# xx = xx.astype('float')
# yy = gen_cosine_amp(x0=0, xn=50000)
# x1 = rnn_data(xx,n_steps)
# y1 = rnn_data(yy,n_steps,labels=True)
# cntx = zip(min5,min10,min20,min30)
# cnty = min10
# cnty = rnn_data(cnty,n_steps,labels=True)
# cntx = rnn_data(cntx,n_steps)

cntx, cnty = generate_data(np.sin, np.linspace(0, 1, 30000), n_steps, seperate=False)
#testx, testy = generate_data(np.sin, np.linspace(10000, 1, 10150), n_steps, seperate=False)
#print cnty.shape,cntx.shape
batch_xs = []
batch_ys = []
tx = cntx['val'][0:batch_size]
ty = cnty['val'][0:batch_size]
step = 1
while step  < 1000:
    batch_xs = cntx['train'][(step-1)*batch_size:step*batch_size]
    batch_ys = cnty['train'][(step-1)*batch_size:step*batch_size]
    sess.run(optimizer, feed_dict={x: batch_xs, y: batch_ys,istate: np.zeros((batch_size, 2*n_hidden))})
    if step % display_step == 0:
        loss = sess.run(cost, feed_dict={x: batch_xs, y: batch_ys, istate: np.zeros((batch_size, 2*n_hidden))})
        txloss = sess.run(cost, feed_dict={x: tx, y: ty, istate: np.zeros((batch_size, 2*n_hidden))})
        print "Iter " + str(step*batch_size) + ", Minibatch Loss= " + "{:.6f}".format(loss) + ", txloss = " + "{:.6f}".format(txloss)
    step = step + 1



print "Iter " + str(step*batch_size) + ", Minibatch Loss= " + "{:.6f}".format(loss)

print "Optimization Finished!"


