#!/usr/bin/python
# coding=utf-8
from __future__ import division
import pandas as pd
import numpy as np
import datetime
import math
import os
import commands
import xgboost as xgb
import pywFM


path = "/Users/simon/Downloads/season_2/test_set_2/"
orderpath = '%s%s'%(path,'order_data/')
os.environ['orderpath']=orderpath
orderlist = commands.getoutput('ls $orderpath | grep order_data')
orderlist = orderlist.split('\n')

######加载order数据##########
order = pd.DataFrame()
for file in orderlist:
	tmp = pd.read_table('%s%s'%(orderpath,file),header=None,names=['oid','sid','uid','fromhash','tohash','price','time'])
	order = order.append(tmp)

order['timebin'] = order['time'].apply(lambda x:datetime.datetime.strptime(x,'%Y-%m-%d %H:%M:%S'))
order['disttime'] = order['timebin'].apply(lambda x:math.floor((x.hour*3600+x.minute*60)/600)+1)
order['date'] = order['timebin'].apply(lambda x:datetime.datetime.strftime(x,'%Y-%m-%d'))
order['minute'] = order['timebin'].apply(lambda x:math.floor((x.hour*60+x.minute))+1)
del order['timebin']
order.to_csv(orderpath+'order_all.csv',index=False)


######加载traffic数据##########
trafficpath = '%s%s'%(path,'traffic_data/')
os.environ['trafficpath']=trafficpath
trafficlist = commands.getoutput('ls $trafficpath | grep traffic_data')
trafficlist = trafficlist.split('\n')
traffic = pd.DataFrame()
for file in trafficlist:
	tmp = pd.read_table('%s%s'%(trafficpath,file),header=None,names=['dhs','level1','level2','level3','level4','time'])
	traffic = traffic.append(tmp)

traffic['timebin'] = traffic['time'].apply(lambda x:datetime.datetime.strptime(x,'%Y-%m-%d %H:%M:%S'))
traffic['disttime'] = traffic['timebin'].apply(lambda x:math.floor((x.hour*3600+x.minute*60)/600)+1)
traffic['time'] = traffic['timebin'].apply(lambda x:datetime.datetime.strftime(x,'%Y-%m-%d'))
del traffic['timebin']
traffic.to_csv(trafficpath+'traffic_all.csv',index=False)

######加载weather数据##########
weatherpath = '%s%s'%(path,'weather_data/')
os.environ['weatherpath']=weatherpath
weatherlist = commands.getoutput('ls $weatherpath | grep weather_data')
weatherlist = weatherlist.split('\n')
weather = pd.DataFrame()
for file in weatherlist:
	tmp = pd.read_table('%s%s'%(weatherpath,file),header=None,names=['time','wdesc','temp','PM'])
	weather = weather.append(tmp)

weather['timebin'] = weather['time'].apply(lambda x:datetime.datetime.strptime(x,'%Y-%m-%d %H:%M:%S'))
weather['disttime'] = weather['timebin'].apply(lambda x:math.floor((x.hour*3600+x.minute*60)/600)+1)
weather['time'] = weather['timebin'].apply(lambda x:datetime.datetime.strftime(x,'%Y-%m-%d'))
del weather['timebin']
weather.to_csv(weatherpath+'weather_all.csv',index=False)

def next_process(torder):
	torder['10min'] = torder['minute'].apply(lambda x:math.floor((x/10))+1)
	torder['5min'] = torder['minute'].apply(lambda x:math.floor((x/5))+1)
	del torder['oid']
	torder['success'] = np.where(torder['sid'].isnull(),0,1)
	torder['cnt'] = 1
	clusterpath = '%s%s'%(path,'cluster_map/')
	did = pd.read_csv(clusterpath+'cluster_map',header=None)
	did.rename(columns={0:'fromhash',1:'fromid'},inplace=True)
	torder = pd.merge(torder,did,on='fromhash',how='left')
	torder['weekday'] = torder['date'].apply(lambda x:datetime.datetime.strptime(x,'%Y-%m-%d').weekday())
	torder['weekday'] = torder['weekday']+1
	torder['weekend'] = 0
	torder['weekend'] = np.where(torder['weekday']==6,2,torder['weekend'])
	torder['weekend'] = np.where(torder['weekday']==7,1,torder['weekend'])
	torder['weekend'] = np.where(torder['weekday']==1,3,torder['weekend'])


# ######加载cluster数据##########
# clusterpath = '%s%s'%(path,'cluster_map/')
# cluster = pd.read_table('%s%s'%(clusterpath,'cluster_map'))
# ######加载poi数据##########
# poipath = '%s%s'%(path,'poi_data/')
# poi = pd.read_table('%s%s'%(poipath,'poi_process'))

