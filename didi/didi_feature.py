%matplotlib osx
from __future__ import division
import pandas as pd
import numpy as np
import datetime
import math
import os
import commands
import xgboost as xgb
import matplotlib as plt
import tensorflow as tf
pd.set_option('display.max_columns', 80)
pd.set_option('display.max_rows', 200)
pd.set_option('expand_frame_repr', False)

path = "/Users/simon/Downloads/didi/middata/"
order = pd.read_csv(path+'ordermid.csv')
# order['10min'] = order['minute'].apply(lambda x:math.floor((x/10))+1)
# order['5min'] = order['minute'].apply(lambda x:math.floor((x/5))+1)
train = order[order.delta<19]
test = order[order.delta>18]

def gbdt_train(dftrain,dftest,dense_feature,target,param,num_round):

	watchlist  = [(xgb.DMatrix(dftest[dense_feature], dftest[target]),'eval'),
	(xgb.DMatrix(dftrain[dense_feature], dftrain[target]),'train')]

	bst = xgb.train(param, xgb.DMatrix(dftrain[dense_feature], dftrain[target]), num_round, watchlist)
	return bst

def get_5_10_td(train):
	fdm_gb5 = train.groupby(['fromid','date','weekend','5min']).agg({'cnt':np.sum,'success':np.sum}).reset_index()
	fdm_gb10 = train.groupby(['fromid','date','weekend','10min']).agg({'cnt':np.sum,'success':np.sum}).reset_index()
	fdm_gb5['gap'] = fdm_gb5['cnt']-fdm_gb5['success']
	fdm_gb10['gap'] = fdm_gb10['cnt']-fdm_gb10['success']
	return fdm_gb5,fdm_gb10

#####用51地区补充######
fdm_gb5,fdm_gb10 = get_5_10_td(train)
# gb51 = fdm_gb10[fdm_gb10.fromid==51]
# gb51_5 = fdm_gb5[fdm_gb5.fromid==51]
# idx10 = gb51[['date','10min']]
# idx5 = gb51_5[['date','5min']]

# tidx10 = pd.DataFrame()
# for i in range(1,67):
# 	tmp = idx10.copy()
# 	tmp['fromid'] = i
# 	tidx10 = tidx10.append(tmp)
# tidx10.to_csv('tidx10.csv',index=False)

# tidx5 = pd.DataFrame()
# for i in range(1,67):
# 	tmp = idx5.copy()
# 	tmp['fromid'] = i
# 	tidx5 = tidx5.append(tmp)
# tidx5.to_csv('tidx5.csv',index=False)

# merge10 = pd.merge(tidx10,fdm_gb10,on=['fromid','date','10min'],how='left')
# idx51 = gb51[['date','10min','weekend']]
# idx51.rename(columns={'weekend':'wkd'},inplace=True)
# merge10 = pd.merge(merge10,idx51,on=['date','10min'],how='left')
# merge10['weekend'] = merge10['wkd']
# del merge10['wkd']
# merge10 = merge10.fillna(0)

def gen_suc_feat(df):
	#cnt = np.array(df['cnt'])
	success = np.array(df['success'])
	fromid = np.array(df['fromid'])
	larray = len(success)
	sucbf1 = np.zeros(larray)
	sucbf2 = np.zeros(larray)
	sucaf1 = np.zeros(larray)

	for i in range(0,larray):
		if i-1>=0:
			if fromid[i]==fromid[i-1]:
				sucbf1[i] = success[i-1]
			else:
				sucbf1[i] = success[i]
		else:
			sucbf1[i] = success[i]

		if i-2>=0:
			if fromid[i]==fromid[i-2]:
				sucbf2[i] = success[i-2]
			else:
				sucbf2[i] = success[i]
		else:
			sucbf2[i] = success[i]

		if i+1<larray:
			if fromid[i]==fromid[i+1]:
				sucaf1[i] = success[i+1]
			else:
				sucaf1[i] = success[i]
		else:
			sucaf1[i] = success[i]
	df['sucbf1'] = sucbf1
	df['sucbf2'] = sucbf2
	df['sucaf1'] = sucaf1

	df['ba30suc'] = (df['sucbf1']+df['success']+df['sucaf1'])/3
	df['b20suc'] = (df['sucbf1']+df['sucbf2'])/2
	df['sucratio'] = df['success']/(df['sucbf1']+1)
	df['sucbratio'] = df['sucbf1']/(df['sucbf2']+1)
	df['sucaratio'] = df['sucaf1']/(df['success']+1)
	return df

def gen_req_feat(df):
	cnt = np.array(df['cnt'])
	#success = np.array(df['success'])
	fromid = np.array(df['fromid'])
	larray = len(cnt)
	cntbf1 = np.zeros(larray)
	cntbf2 = np.zeros(larray)
	cntaf1 = np.zeros(larray)
	# sucbf1 = np.zeros(larray)
	# sucbf2 = np.zeros(larray)
	# sucaf1 = np.zeros(larray)

	for i in range(0,larray):
		if i-1>=0:
			if fromid[i]==fromid[i-1]:
				cntbf1[i] = cnt[i-1]
				#sucbf1[i] = success[i-1]
			else:
				cntbf1[i] = cnt[i]
				#sucbf1[i] = success[i]
		else:
			cntbf1[i] = cnt[i]
			#sucbf1[i] = success[i]

		if i-2>=0:
			if fromid[i]==fromid[i-2]:
				cntbf2[i] = cnt[i-2]
				#sucbf2[i] = success[i-2]
			else:
				cntbf2[i] = cnt[i]
				#sucbf2[i] = success[i]
		else:
			cntbf2[i] = cnt[i]
			#sucbf2[i] = success[i]

		if i+1<larray:
			if fromid[i]==fromid[i+1]:
				cntaf1[i] = cnt[i+1]
				#sucaf1[i] = success[i+1]
			else:
				cntaf1[i] = cnt[i]
				#sucaf1[i] = success[i]
		else:
			cntaf1[i] = cnt[i]
			#sucaf1[i] = success[i]
	df['cntbf1'] = cntbf1
	df['cntbf2'] = cntbf2
	df['cntaf1'] = cntaf1

	df['ba30cnt'] = (df['cntbf1']+df['cnt']+df['cntaf1'])/3
	df['b20cnt'] = (df['cntbf1']+df['cntbf2'])/2
	df['cntratio'] = df['cnt']/(df['cntbf1']+1)
	df['cntbratio'] = df['cntbf1']/(df['cntbf2']+1)
	df['cntaratio'] = df['cntaf1']/(df['cnt']+1)
	return df
merge10 = gen_req_feat(merge10)

# merge10['ba30suc'] = (merge10['sucbf1']+merge10['success']+merge10['sucaf1'])/3
# merge10['b20suc'] = (merge10['sucbf1']+merge10['sucbf2'])/2
# merge10['sucratio'] = merge10['success']/(merge10['sucbf1']+1)
# merge10['sucbratio'] = merge10['sucbf1']/(merge10['sucbf2']+1)
# merge10['sucaratio'] = merge10['sucaf1']/(merge10['success']+1)
tmp = merge10.groupby(['fromid','weekend','10min'])['cntaf1','cnt','cntbf1','b20cnt','ba30cnt','cntratio','cntbratio','cntaratio'].agg({'mean':np.mean,'std':np.std}).reset_index()
tmp.columns = [' '.join(col).strip() for col in tmp.columns.values]
tmppart = tmp[['fromid','weekend','10min','mean cntaf1','mean cnt','std cnt','mean cntbf1','std cntbf1','mean b20cnt','std b20cnt','mean ba30cnt','std ba30cnt','mean cntratio','mean cntbratio','std cntbratio','mean cntaratio']]
merge1 = pd.merge(merge10,tmppart,on=['fromid','weekend','10min'],how='left')
#merge1['cntnor'] = (merge1['cnt']-merge1['mean cnt'])/(merge1['std cnt']+0.00001)
merge1['b10cntnor'] = (merge1['cntbf1']-merge1['mean cntbf1'])/(merge1['std cntbf1']+0.00001)
merge1['b20cntnor'] = (merge1['b20cnt']-merge1['mean b20cnt'])/(merge1['std b20cnt']+0.00001)
merge1['cntbrationor'] = (merge1['cntbratio']/(merge1['mean cntbratio']+0.00001)
merge1['pcnt'] = merge1['cntbf1']*(merge1['cntbratio']+merge1['mean cntratio'])/2
merge1 = merge1[(merge1['10min']>2)&(merge1['10min']<145)]


mergesuc10 = gen_suc_feat(merge10)
tmpsuc = mergesuc10.groupby(['fromid','weekend','10min'])['cntaf1','cnt','cntbf1','sucaf1','success','sucbf1','b20suc','ba30suc','sucratio','sucbratio','sucaratio'].agg({'mean':np.mean,'smax':'max','std':np.std}).reset_index()
tmpsuc.columns = [' '.join(col).strip() for col in tmpsuc.columns.values]
tmppartsuc = tmpsuc[['fromid','weekend','10min','mean cntaf1','mean cnt','std cnt','mean cntbf1','mean sucaf1','std sucaf1','smax sucaf1','mean success','std success','smax success','mean sucbf1','std sucbf1','smax sucbf1','mean b20suc','std b20suc','mean ba30suc','mean sucratio','mean sucbratio','mean sucaratio']]
mergesuc1 = pd.merge(mergesuc10,tmppartsuc,on=['fromid','weekend','10min'],how='left')
mergesuc1['b10sucnor'] = (mergesuc1['sucbf1']-mergesuc1['mean sucbf1'])/(mergesuc1['std sucbf1']+0.00001)
mergesuc1['b20sucnor'] = (mergesuc1['b20suc']-mergesuc1['mean b20suc'])/(mergesuc1['std b20suc']+0.00001)
mergesuc1['sucbrationor'] = mergesuc1['sucbratio']/(mergesuc1['mean sucbratio']+0.0000001)
mergesuc1['psuc'] = mergesuc1['sucbf1']*(mergesuc1['sucbratio']+mergesuc1['mean sucratio'])/2
suc_dense_feature = ['weekend','cntbf1','sucbf1','sucbf2','b20suc','sucbratio','mean cnt','std cnt','mean cntbf1','mean sucaf1','std sucaf1','smax sucaf1','mean success','std success','smax success','mean sucbf1','std sucbf1','smax sucbf1','mean b20suc','mean ba30suc','mean sucratio','mean sucbratio','mean sucaratio','b10sucnor','b20sucnor','sucbrationor','psuc']
mergesuc1 = mergesuc1.iloc[np.random.permutation(len(mergesuc1))]
strain = mergesuc1[:160000]
sval = mergesuc1[160000:]
param_tree = {"objective": "reg:linear",
          "eta": 0.1,
          "max_depth": 8,
          "min_child_weight": 3,
          "subsample":0.8,
          "colsample_bytree":0.9,
          "booster":"gbtree",
          "eval_metric":"rmse"
	}
num_round_tree = 80
model_tree = gbdt_train(strain,suc_mergetest10,suc_dense_feature,'success',param_tree,num_round_tree)
param_liner = {"objective": "reg:linear",
	"eta": 0.15,
	"booster ":"gblinear",
	"lambda":0.1,
	"eval_metric":"rmse",
}
num_round_linear = 50
model_linear = gbdt_train(strain,suc_mergetest10,suc_dense_feature,'success',param_liner,num_round_linear)
###################################################




alldist = merge1.fromid.unique()
bigdist = [23,7,8,1,51,48]
middist = [37,46,28,12,14,24,42,27,20,21,19,4]
mindist = list(set(alldist)^(set(bigdist)|set(middist)))
merge1big = merge1[(merge1.fromid.isin(bigdist))]
merge1mid = merge1[(merge1.fromid.isin(middist))]
merge1min = merge1[(merge1.fromid.isin(mindist))]
merge1 = merge1.iloc[np.random.permutation(len(merge1))]
train = merge1[:160000]
val = merge1[160000:]
param_tree = {"objective": "reg:linear",
          "eta": 0.1,
          "max_depth": 7,
          "min_child_weight": 3,
          "subsample":1,
          "colsample_bytree":1,
          "booster":"gbtree",
          "eval_metric":"rmse"
	}
num_round_tree = 100
dense_feature = ['weekend','cntbf1','cntbf2','b20cnt','mean cntaf1','mean cnt','mean cntbf1','std cntbf1','mean b20cnt','mean ba30cnt','std cnt','b10cntnor','b20cntnor','cntbrationor','mean cntratio','mean cntbratio','mean cntaratio','pcnt']
model_tree = gbdt_train(merge1min,reqtestmin,dense_feature,'cnt',param_tree,num_round_tree)

param_liner = {"objective": "reg:linear",
	"eta": 0.1,
	"booster ":"gblinear",
	"lambda":0.1,
	"eval_metric":"rmse",
}
num_round_linear = 100
model_linear = gbdt_train(merge1big,reqtestbig,dense_feature,'cnt',param_liner,num_round_linear)





resultl = model_linear.predict(xgb.DMatrix(mergetest10[dense_feature]))
resultt = model_tree.predict(xgb.DMatrix(mergetest10[dense_feature]))
mergetest10['p1'] = resultl
mergetest10['p2'] = resultt
########做测试集##################
test10 = test.groupby(['fromid','date','weekend','10min']).agg({'cnt':np.sum,'success':np.sum}).reset_index()
test10 = gen_req_feat(test10)
mergetest10 = pd.merge(test10,tmppart,on=['fromid','weekend','10min'],how='left')
mergetest10['b10cntnor'] = (mergetest10['cntbf1']-mergetest10['mean cntbf1'])/(mergetest10['std cntbf1']+0.00001)
mergetest10['b20cntnor'] = (mergetest10['b20cnt']-mergetest10['mean b20cnt'])/(mergetest10['std b20cnt']+0.00001)
mergetest10['cntbrationor'] = (mergetest10['cntbratio']-mergetest10['mean cntbratio'])/(mergetest10['std cntbratio']+0.00001)
mergetest10['pcnt'] = mergetest10['cntbf1']*(mergetest10['cntbratio']+mergetest10['mean cntratio'])/2
mergetest10 = mergetest10[(mergetest10['10min']>2)&(mergetest10['10min']<145)]
reqtestbig = mergetest10[(mergetest10.fromid.isin(bigdist))]
reqtestmid = mergetest10[(mergetest10.fromid.isin(middist))]
reqtestmin= mergetest10[(mergetest10.fromid.isin(mindist))]
reqtestbig['predl'] = model_linear.predict(xgb.DMatrix(reqtestbig[dense_feature]))
reqtestmin['predl'] = model_linear.predict(xgb.DMatrix(reqtestmin[dense_feature]))



suc_test10 = gen_suc_feat(test10)
suc_mergetest10 = pd.merge(suc_test10,tmppartsuc,on=['fromid','weekend','10min'],how='left')
suc_mergetest10['b10sucnor'] = (suc_mergetest10['sucbf1']-suc_mergetest10['mean sucbf1'])/(suc_mergetest10['std sucbf1']+0.00001)
suc_mergetest10['b20sucnor'] = (suc_mergetest10['b20suc']-suc_mergetest10['mean b20suc'])/(suc_mergetest10['std b20suc']+0.00001)
suc_mergetest10['sucbrationor'] = suc_mergetest10['sucbratio']/(suc_mergetest10['mean sucbratio']+0.0000001)
suc_mergetest10['psuc'] = suc_mergetest10['sucbf1']*(suc_mergetest10['sucbratio']+suc_mergetest10['mean sucratio'])/2
suc_mergetest10 = suc_mergetest10[(suc_mergetest10['10min']>2)&(suc_mergetest10['10min']<145)]







####生成与上个10分钟的比值



# tmprow = []
# k = 1
# for idx,row in tmp.iterrows():
# 	k = k+1
# 	if k>100:
# 		break
# 	print idx
# 	if idx==0:
# 		tmprow = row
# 		continue
# 	if[tmp.fromid[idx]==tmp.fromid[idx-1]]:
# 		tmp.bfcntratio[idx] = row['mean cnt']/(tmprow['mean cnt']+0.0000001)
# 		tmp.bfsucratio[idx] = row['mean success']/(tmprow['mean success']+0.0000001)
# 		tmprow = row
# 	else:
# 		tmprow = row
# 		continue

m51 = merge1[merge1.fromid==51]
m51x = m51[dense_feature]
cntx = m51x.values
cnty = m51['cnt'].values
trainx = []
trainy = []
for i in range(0,len(cnty)-TIMESTEPS):
    tmpx = []
    for j in range(0,TIMESTEPS):
        tmpx.append(cntx[i+j])
    trainx.append(tmpx)
    trainy.append(cnty[i])


tm51 = mergetest10[mergetest10.fromid==51]
tm51x = tm51[dense_feature]
tntx = tm51x.values
tnty = tm51['cnt'].values
testx = []
testy = []
for i in range(0,len(tnty)-TIMESTEPS):
    tmpx = []
    for j in range(0,TIMESTEPS):
        tmpx.append(tntx[i+j])
    testx.append(tmpx)
    testy.append(tnty[i])

trainx = np.array(trainx)
trainy = np.array(trainy)
testx = np.array(testx)
testy = np.array(testy)













