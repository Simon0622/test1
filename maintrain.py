import pandas as pd
import numpy as np
import xgboost as xgb
import datetime
import pickle
from sklearn.ensemble import RandomForestClassifier

def sparse(train,test,onehots,target):
    cols = train.columns
    dens_feat = list(set(cols)^set(onehots))
    dens_feat = list(set(dens_feat)^set([target]))

    train_array = train[dens_feat].as_matrix()
    test_array = test[dens_feat].as_matrix()
    if target in test.columns:
    	return train_array,train[target],test_array,test[target]
    else:
        return train_array,train[target],test_array

if __name__ == '__main__':
    path = '/Users/simon/Downloads/tianchi'
    yjh = pd.read_csv(path+'/result/yjh.csv')
    yjh = yjh.ix[:,['User_id','Location_id','Merchant_id','result']]
    cvtest = pd.read_csv(path+'/rawdata/ijcai2016_koubei_test')
    cvtest = pd.merge(cvtest,yjh,on=['User_id','Location_id'],how='left')
    wjh = cvtest[cvtest['result'].isnull()].ix[:,['User_id','Location_id']]

    print str(wjh.shape)

    kbtrain = pd.read_csv(path+'/middata/kbtrain.csv')
    train0 = kbtrain[kbtrain['label']==0].sample(frac=0.25)
    train1 = kbtrain[kbtrain['label']==1]
    kbtrain = train0.append(train1)
    cv = pd.read_csv(path+'/middata/cvtest.csv')

    kbtest = pd.read_csv(path+'/middata/kbtest.csv')
    wjh = pd.merge(wjh,kbtest,on=['Location_id'],how='inner')
    print str(wjh.shape)

    onehots = ['User_id','Location_id','Merchant_id','mstartday']
    target = 'label'
    dens_feat = list(set(kbtrain.columns)^set(onehots))
    dens_feat = list(set(dens_feat)^set([target]))

    # print "RandomForestClassifier train  begin"
    # rf = RandomForestClassifier(n_estimators=25, n_jobs = -1, random_state=1)
    # rf.fit(kbtrain[dens_feat], kbtrain[target])
    # joblib.dump(rf, path+'/model/rf.pkl')
    # print "RandomForestClassifier train end"

    params = {"objective": "binary:logistic",
          "eta": 0.1,
          "max_depth": 5,
          "min_child_weight": 3,
          "subsample": 0.7,
          "colsample_bytree": 0.7,
          "scale_pos_weight":train0.shape[0]/train1.shape[0]
    }
    watchlist  = [(xgb.DMatrix(cv[dens_feat], cv[target]),'eval'),(xgb.DMatrix(kbtrain[dens_feat], kbtrain[target]),'train')]
    bst = xgb.train(params, xgb.DMatrix(kbtrain[dens_feat], kbtrain[target]), 10, watchlist)
    bst.save_model(path+'/model/gbm0513.gbdt')

    preds = bst.predict(xgb.DMatrix(wjh[dens_feat]))
    wjh['result'] = preds
    wjh = wjh.ix[:,['User_id','Location_id','Merchant_id','result']]
    ulgb = wjh.groupby(['User_id','Location_id']).agg({'result':'max'})
    ulgb = pd.merge(ulgb,wjh,on=['User_id','Location_id','result'],how='inner')
    ulgb.to_csv(path+'/result/wjh.csv',index=False)



