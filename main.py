#!/usr/bin/python
# coding=utf-8
from __future__ import division
from math import log
import numpy as np
import pandas as pd
from DealNa import *
from LongTailProcess import LongTailProcessDorpNA
from Smooth import SmoothwithBeta
from Discreate import *
from GenerateRTB import GenerateRTB_BinClass
from Format import FormatGBDT
from Format import FormatFFM
#import lffm
from Normalized import *

#import pydevd
#pydevd.settrace('172.16.25.162', port=8123, stdoutToServer=True, stderrToServer=True)


def transform(x):
    return x


if __name__ == '__main__':
    train = pd.read_csv('titanic/train.csv')
    test = pd.read_csv('titanic/test.csv')
    del train['PassengerId']
    del train['Ticket']
    del train['Name']
    tpid =  test['PassengerId']
    del test['PassengerId']
    del test['Ticket']
    del test['Name']
    ####直接去掉为空的值
    dealNa = DealNaB()
    train = dealNa.deal_na(train, target='Survived')
    test = dealNa.deal_na(test)
    #####处理长尾特征
    # ltProcess = LongTailProcessDorpNA()
    # ltProcess.long_tail(df,[],'iscl')

    # #####GBDT离散化#####
    #######不需要离散化

    #####FFM离散化#####
    discreate = DiscreateA()
    train, test = discreate.discreate(train, test, ['Age', 'Fare', 'SibSp', 'Parch'], 'Survived', transform,
                                      isReplace=False)

    # ####GBDT产生RTB特征
    # grtb = GenerateRTB_BinClass()
    # train, test = grtb.generateRTB(train, test, target='Survived', tarnum=1,
    #                                cols=['Pclass', 'Sex', 'Embarked'])
    ####FFM产生RTB特征
    grtb = GenerateRTB_BinClass()
    train, test = grtb.generateRTB(train, test, target='Survived', tarnum=1,
                                   cols=['Pclass', 'Sex', 'Embarked', 'Agebin', 'Farebin', 'SibSpbin', 'Parchbin'])


    formatGBDT = FormatGBDT({})
    map = formatGBDT.onehot_coding(train,['Pclass','Sex','Embarked'],'Survived')
    formatGBDT.write_fmap(map, train, 'Survived', 'titanic/titanic.fmap')
    csrtrain, labels = formatGBDT.to_scipy_sparse(train, map,'Survived')
    csrtest, tlabels = formatGBDT.to_scipy_sparse(test, map)
    #formatGBDT.format(df,['Pclass','Name','Sex','Age','SibSp','Parch','Embarked'],'Survived')
    #csr, labels = formatGBDT.to_scipy_sparse(df,'Survived')
    preds = formatGBDT.train(csrtrain,csrtest,labels,
                     param={'max_depth': 3, 'eta': 0.1, 'objective': 'binary:logistic'},num_round = 50)

    result = pd.DataFrame({'PassengerId':tpid,'Survived':preds})
    result['Survived'] = result['Survived'].apply(lambda x:int(x>0.5))
    result.to_csv('result.csv',index=False)


    #onehot_args = ['Pclass', 'Sex', 'Age', 'SibSp', 'Parch', 'Fare', 'Embarked']
    # normoalize = NormalizeA()
    # train = normoalize.normalize(train, onehot_args,'Survived')
    # test = normoalize.normalize(test, onehot_args)
    # test = dealNa.deal_na(test)
    #
    # formatFFM = FormatFFM({})
    # map = formatFFM.onehot_coding(train, onehot_args, 'Survived')
    # formatFFM.json_obj(map, 'ffm.map')
    # formatFFM.write_ffm_file(train, map,
    #                          {'fare': ['Pclass', 'Fare'],
    #                           'people': ['Sex', 'Age'],
    #                           'relate': ['SibSp', 'Parch'],
    #                           'Embarked': ['Embarked'],
    #                           'RTB': ['Pclassrtb', 'Sexrtb', 'Embarkedrtb', 'Agertb', 'Farertb', 'SibSprtb',
    #                                   'Parchrtb']},
    #                          'ffm.dat',
    #                          target = 'Survived')
    # formatFFM.write_ffm_file(test, map,
    #                          {'fare': ['Pclass', 'Fare'],
    #                           'people': ['Sex', 'Age'],
    #                           'relate': ['SibSp', 'Parch'],
    #                           'Embarked': ['Embarked'],
    #                           'RTB': ['Pclassrtb', 'Sexrtb', 'Embarkedrtb', 'Agertb', 'Farertb', 'SibSprtb',
    #                                   'Parchrtb']},
    #                          'ffm.test.dat')
    # lffm.train('-l 0.0005 -k 4 -t 500 --auto-stop -r 0.05 ffm.dat ffm.model')
    # lffm.predict('ffm.test.dat ffm.model output')
