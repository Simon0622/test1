#!/usr/bin/python
# coding=utf-8
from __future__ import division
from math import log
import numpy as np
import pandas as pd
from math import log


class Discreate():
    def discreate(self, df, cols,target,tran_func):
        return

    def getEnt(self, pargs):
        sum = 0
        for i in pargs:
            sum += i * log(i, 2)
        return -sum

    def computeConEnt(self, col, tsum, df, target):
        # 求条件熵
        gb = df.groupby(by=[col, target]).size()
        gb = gb.reset_index()
        gb.rename(columns={0: 'size'}, inplace=True)
        # 列出该列所有取值
        nodelist = df[col].unique().tolist()
        sumConEnt = 0
        for i in nodelist:
            gbi = gb[gb[col] == i]
            sum = gbi['size'].sum()
            pi = sum / tsum
            gbi['cpi'] = gbi['size'] / sum
            l = [gbi['cpi']]
            pargs = np.array(l, dtype=pd.Series)
            conEnt = self.getEnt(pargs[0])
            sumConEnt = sumConEnt + conEnt * pi

        return sumConEnt


class DiscreateA(Discreate):
    def discreate(self, df, dftest,cols, target,tran_func,isReplace = False):
        len = df.shape[0]
        # 求条件熵
        ##需要修改pandas里面tile.py文件，qcut有bug
        for col in cols:
            cut = 8
            # 初始化一个特别大的值
            colcoLast = 10000
            df[col] = df[col].apply(lambda x:tran_func(x))
            while cut >= 4:
                df['bin'], dftest['bin'] = pd.qcut(df[col], dftest[col], cut)
                #print pd.qcut(df[col],cut).value_counts()
                df['binid'] = pd.factorize(df['bin'])[0]
                colcoEnt = self.computeConEnt('binid', len, df, target)
                if colcoEnt < colcoLast:
                    colcoLast = colcoEnt
                    if isReplace:
                        df[col] = df['binid']
                        dftest[col] = pd.factorize(dftest['bin'])[0]
                    else:
                        df[col+'bin'] = df['binid']
                        dftest[col+'bin'] = pd.factorize(dftest['bin'])[0]
                cut = cut - 1

        del df['bin']
        del dftest['bin']
        del df['binid']

        return df,dftest


class DiscreateB(Discreate):
    def discreate(self, df, dftest, cols, target,tran_func):
        len = df.shape[0]
        # 求条件熵
        ##需要修改pandas里面tile.py文件，qcut有bug
        for col in cols:
            cut = 8
            # 初始化一个特别大的值
            colcoLast = 10000
            df[col] = df[col].apply(lambda x:tran_func(x))
            while cut >= 4:
                df['bin'], dftest['bin'] = pd.qcut(df[col], dftest[col], cut)
                #print pd.qcut(df[col],cut).value_counts()
                df['binid'] = pd.factorize(df['bin'])[0]
                colcoEnt = self.computeConEnt('binid', len, df, target)
                if colcoEnt < colcoLast:
                    colcoLast = colcoEnt
                    df[col] = df['binid']
                    dftest[col+'binid'] = pd.factorize(dftest['bin'])[0]
                cut = cut - 1
        del df['bin']
        #del df['binid']
        del dftest['bin']
        #del dftest['binid']

        return df,dftest


if __name__ == '__main__':
    dis = DiscreateA()
    # df = pd.read_csv('D:/paper/csample.csv')
    # dis.discreate(df, ['cctr', 'actr'], 'realcl')
