#!/usr/bin/python
# coding=utf-8
import pandas as pd
import numpy as np
import datetime
path = '/Users/simon/Downloads/tianchi/'
tb = pd.read_csv(path+'rawdata/ijcai2016_taobao')
####分别抽取训练样本和测试样本
###用户不区分，全部用train
train = tb[tb['Time_Stamp']<=20151130]
#test = tb[tb['Time_Stamp']>=20150715]

train = train[['User_id','Item_id','Online_Action_id']]
#test = test[['User_id','Item_id','Online_Action_id']]

#####train cvr特征
train['cnt'] = 1
train_cvr = train.groupby(['User_id']).agg({'Online_Action_id':np.sum,'cnt':np.sum})
train_cvr = train_cvr.reset_index()
train_cvr.rename(columns={'Online_Action_id':'buycnt'},inplace=True)
train_cvr['buyratio'] = train_cvr['buycnt']/(train_cvr['cnt']+1)
#train_cvr.to_csv('train_cvr.csv',index=False)
#####做train set第一表特征
train_igb = train.groupby(['Item_id','Online_Action_id']).size().reset_index()
train_igb.rename(columns={0:'size'},inplace=True)
train_igb0 = train_igb[train_igb['Online_Action_id']==0]
train_igb1 = train_igb[train_igb['Online_Action_id']==1]
del train_igb0['Online_Action_id']
del train_igb1['Online_Action_id']
train_igb0.rename(columns={'size':'view'},inplace=True)
train_igb1.rename(columns={'size':'buy'},inplace=True)
igb = train.groupby('Item_id').size().reset_index()
igb = pd.merge(igb,train_igb0,on=['Item_id'],how='left')
train_td1 = pd.merge(igb,train_igb1,on=['Item_id'],how='left')
train_td1.to_csv(path+'middata/train_tb1.csv',index=False)
#####做train set第二表特征
train = pd.merge(train,train_td1,on=['Item_id'],how='left')
#del train['Online_Action_id']
train['buy'] = np.where(train['Online_Action_id']==0,np.nan,train['buy'])
train['view'] = np.where(train['Online_Action_id']==1,np.nan,train['view'])
train_tb2 = train.groupby('User_id').agg({'view':np.mean,'buy':np.mean})
train_tb2 = train_tb2.reset_index()
train_tb3 = train.groupby('User_id').agg({'view':np.median,'buy':np.median})
train_tb3 = train_tb3.reset_index()
train_tb3.rename(columns={'view':'viewzz','buy':'buyzz'},inplace=True)
train_tb2 = pd.merge(train_tb2,train_tb3,on=['User_id'],how='left')
train_tb2 = pd.merge(train_tb2,train_cvr,on=['User_id'],how='left')
train_tb2 = train_tb2.fillna(0)
train_tb2.to_csv(path+'middata/train_tb2.csv',index=False)


#####test cvr特征
# test['cnt'] = 1
# test_cvr = test.groupby(['User_id']).agg({'Online_Action_id':np.sum,'cnt':np.sum})
# test_cvr = test_cvr.reset_index()
# test_cvr.rename(columns={'Online_Action_id':'buycnt'},inplace=True)
# test_cvr['buyratio'] = test_cvr['buycnt']/(test_cvr['cnt']+1)
# #test_cvr.to_csv('test_cvr.csv',index=False)
# #####做test set第一表特征
# test_igb = test.groupby(['Item_id','Online_Action_id']).size()
# test_igb = test_igb.reset_index()
# test_igb.rename(columns={0:'size'},inplace=True)
# test_igb0 = test_igb[test_igb['Online_Action_id']==0]
# test_igb1 = test_igb[test_igb['Online_Action_id']==1]
# del test_igb0['Online_Action_id']
# del test_igb1['Online_Action_id']
# test_igb0.rename(columns={'size':'view'},inplace=True)
# test_igb1.rename(columns={'size':'buy'},inplace=True)
# igb = test.groupby('Item_id').size()
# igb=igb.reset_index()
# igb = pd.merge(igb,test_igb0,on=['Item_id'],how='left')
# test_tb1 = pd.merge(igb,test_igb1,on=['Item_id'],how='left')
# test_tb1.to_csv('middata/test_tb1.csv',index=False)
# #####做test set第二表特征
# test = pd.merge(test,test_tb1,on=['Item_id'],how='left')
# test['buy'] = np.where(test['Online_Action_id']==0,np.nan,test['buy'])
# test['view'] = np.where(test['Online_Action_id']==1,np.nan,test['view'])
# test_tb2 = test.groupby('User_id').agg({'view':np.mean,'buy':np.mean})
# test_tb2 = test_tb2.reset_index()
# test_tb3 = test.groupby('User_id').agg({'view':np.median,'buy':np.median})
# test_tb3 = test_tb3.reset_index()
# test_tb3.rename(columns={'view':'viewzz','buy':'buyzz'},inplace=True)
# test_tb2 = pd.merge(test_tb2,test_tb3,on=['User_id'],how='left')
# test_tb2 = pd.merge(test_tb2,test_cvr,on=['User_id'],how='left')
# test_tb2.to_csv('middata/test_tb2.csv',index=False)

gb = tb.groupby(['User_id','Category_id','Online_Action_id']).size()
tb['Online_Action_id'] = tb['Online_Action_id']*4
tb['Online_Action_id'] = tb['Online_Action_id']+1
gb = tb.groupby(['User_id','Category_id']).agg({'Online_Action_id':np.sum})
gb = gb.reset_index()
gbpvt = gb.pivot_table('Online_Action_id',['User_id'],'Category_id')
gbpvt = gbpvt.fillna(0)
gbpvt = gbpvt.reset_index()
gbpvt.to_csv(path+'middata/gbpvt.csv',index=False)



gb = tb.groupby(['User_id','Seller_id','Online_Action_id']).size()
tb['Online_Action_id'] = tb['Online_Action_id']*4
tb['Online_Action_id'] = tb['Online_Action_id']+1
gb = tb.groupby(['User_id','Seller_id']).agg({'Online_Action_id':np.sum})
gb = gb.reset_index()
gbpvt = gb.pivot_table('Online_Action_id',['User_id'],'Seller_id')
gbpvt = gbpvt.fillna(0)
gbpvt = gbpvt.reset_index()
gbpvt.to_csv(path+'middata/gbspvt.csv',index=False)






# kbtrain = pd.read_csv(path+'middata/kbtrain.csv')
# kbtrain = pd.merge(kbtrain,train_tb2,on=['User_id'],how='inner')
# kbtrain = kbtrain.dropna()
# #kbtrain = pd.merge(kbtrain,gbpvt,on=['User_id'],how='inner')
# kbtrain.to_csv(path+'middata/kbtrain.csv',index=False)

# cvtest = pd.read_csv(path+'middata/cvtest.csv')
# cvtest = pd.merge(cvtest,train_tb2,on=['User_id'],how='inner')
# cvtest = cvtest.dropna()
# #cvtest = pd.merge(cvtest,gbpvt,on=['User_id'],how='inner')
# cvtest.to_csv(path+'middata/cvtest.csv',index=False)

kbtest = pd.read_csv(path+'middata/kbtest.csv')
kbtest = pd.merge(kbtest,test_tb2,on=['User_id'],how='inner')
####表示先单独训练有交互用户
kbtest = kbtest.dropna()
#kbtest = pd.merge(kbtest,gbpvt,on=['User_id'],how='inner')
kbtest.to_csv(path+'middata/kbtest.csv',index=False)




# ###koubei table
# kb = pd.read_csv('dist.csv')
# mrt = pd.read_csv('ijcai2016_merchant_info.deal')
# mrt.rename(columns={'Location_id_list':'Location_id'},inplace=True)

# # mtrain = pd.merge(kb,train_tb2,on=['User_id'],how='left')
# # mtrain = mtrain.groupby(['Location_id','Merchant_id']).agg({'buy':np.mean,'view':np.mean,'buyzz':np.mean,'viewzz':np.mean,'buycnt':np.mean,'cnt':np.mean,'buyratio':np.mean})
# # mtrain = mtrain.reset_index()

# # mtest = pd.merge(kb,test_tb2,on=['User_id'],how='left')
# # mtest = mtest.groupby(['Location_id','Merchant_id']).agg({'buy':np.mean,'view':np.mean,'buyzz':np.mean,'viewzz':np.mean,'buycnt':np.mean,'cnt':np.mean,'buyratio':np.mean})
# # mtest = mtest.reset_index()
# # mtrain = mtrain.fillna(-20)
# # mtest = mtest.fillna(-20)
# # mtrain.to_csv('mtrain.csv',index=False)
# # mtest.to_csv('mtest.csv',index=False)


# kbtrain = kb[kb['ts']<='2015-10-31']
# kbtrain['mstartday'] = kbtrain['mstartday'].apply(lambda x:datetime.datetime.strptime(x,'%Y-%m-%d'))
# kbtrain['tmp'] = datetime.datetime.strptime('20151031','%Y%m%d')-kbtrain['mstartday']
# kbtrain['m_exist_days'] = kbtrain['tmp'].apply(lambda x:(x / np.timedelta64(1, 'D')).astype(int)+1)
# del kbtrain['tmp']

# kbtest = kb[kb['ts']>='2015-08-01']
# kbtest['mstartday'] = kbtest['mstartday'].apply(lambda x:datetime.datetime.strptime(x,'%Y-%m-%d'))
# kbtest['tmp'] = datetime.datetime.strptime('20151130','%Y%m%d')-kbtest['mstartday']
# kbtest['m_exist_days'] = kbtest['tmp'].apply(lambda x:(x / np.timedelta64(1, 'D')).astype(int)+1)
# del kbtest['tmp']

# #####kb train集
# ugbtrain = kbtrain.groupby(['Merchant_id','Location_id','User_id']).size()
# ugbtrain = ugbtrain.reset_index()
# ugbtrain.rename(columns={0:'cnt'},inplace=True)
# ugbtrain['ucnt'] = 1
# ugbtrain = ugbtrain.groupby(['Merchant_id','Location_id']).agg({'cnt':np.sum,'ucnt':np.sum})
# ugbtrain = ugbtrain.reset_index()

# ugbtrain3 = kbtrain[kbtrain['ts']<='2015-10-27'].groupby(['Merchant_id','Location_id','User_id']).size()
# ugbtrain3 = ugbtrain3.reset_index()
# ugbtrain3.rename(columns={0:'3cnt'},inplace=True)
# ugbtrain3['3ucnt'] = 1
# ugbtrain3 = ugbtrain3.groupby(['Merchant_id','Location_id']).agg({'3cnt':np.sum,'3ucnt':np.sum})
# ugbtrain3 = ugbtrain3.reset_index()

# ugbtrain7 = kbtrain[kbtrain['ts']<='2015-10-24'].groupby(['Merchant_id','Location_id','User_id']).size()
# ugbtrain7 = ugbtrain7.reset_index()
# ugbtrain7.rename(columns={0:'7cnt'},inplace=True)
# ugbtrain7['7ucnt'] = 1
# ugbtrain7 = ugbtrain7.groupby(['Merchant_id','Location_id']).agg({'7cnt':np.sum,'7ucnt':np.sum})
# ugbtrain7 = ugbtrain7.reset_index()

# ugbtrain15 = kbtrain[kbtrain['ts']<='2015-10-16'].groupby(['Merchant_id','Location_id','User_id']).size()
# ugbtrain15 = ugbtrain15.reset_index()
# ugbtrain15.rename(columns={0:'15cnt'},inplace=True)
# ugbtrain15['15ucnt'] = 1
# ugbtrain15 = ugbtrain15.groupby(['Merchant_id','Location_id']).agg({'15cnt':np.sum,'15ucnt':np.sum})
# ugbtrain15 = ugbtrain15.reset_index()

# ugbtrain = pd.merge(ugbtrain,ugbtrain3,on=['Merchant_id','Location_id'],how='left')
# ugbtrain = pd.merge(ugbtrain,ugbtrain7,on=['Merchant_id','Location_id'],how='left')
# ugbtrain = pd.merge(ugbtrain,ugbtrain15,on=['Merchant_id','Location_id'],how='left')

# ugbtrain = pd.merge(ugbtrain,mrt,on=['Merchant_id','Location_id'],how='left')
# ugbtrain = ugbtrain.fillna(0)
# ugbtrain['bgt'] = ugbtrain['Budget']/100
# ugbtrain['bgt'] = ugbtrain['bgt'].apply(lambda x:round(x))

# ugbtrain_bgt_gb = ugbtrain.groupby(['bgt']).agg({'cnt':np.mean,'ucnt':np.mean})
# ugbtrain_bgt_gb = ugbtrain_bgt_gb.reset_index()
# ugbtrain_bgt_gb.rename(columns={'cnt':'bcntmean','ucnt':'bucntmean'},inplace=True)
# ugbtrain = pd.merge(ugbtrain,ugbtrain_bgt_gb,on=['bgt'],how='left')

# kbtrainsubcol = kbtrain.groupby(['Location_id','Merchant_id','ts']).agg({'m_exist_days':'max'})
# kbtrainsubcol = kbtrainsubcol.reset_index()
# kbtrainsubcol['buyday'] = 1
# kbtrainsubcol = kbtrainsubcol.groupby(['Location_id','Merchant_id']).agg({'m_exist_days':'max','buyday':np.sum})
# kbtrainsubcol = kbtrainsubcol.reset_index()
# kbtrainsubcol['nobuyday'] = kbtrainsubcol['m_exist_days']-kbtrainsubcol['buyday']
# kbtrainsubcol['buycntratio'] = kbtrainsubcol['buyday']/kbtrainsubcol['m_exist_days']


# mlcount = kbtrain.groupby(['Location_id','Merchant_id','ts']).size()
# mlcount = mlcount.reset_index()
# mlcount.rename(columns={0:'sizemax'},inplace=True)
# mlcount['sizemin'] = mlcount['sizemax']
# mlcount['sizemean'] = mlcount['sizemax']
# mlcount['sizemed'] = mlcount['sizemax']
# mlcount['sizestd'] = mlcount['sizemax']
# mlcountgb = mlcount.groupby(['Location_id','Merchant_id']).agg({'sizemax':'max','sizemin':'min','sizemean':np.mean,'sizemed':np.median,'sizestd':np.std})
# mlcountgb = mlcountgb.reset_index()

# kbtrainsubcol = pd.merge(kbtrainsubcol,mlcountgb,on=['Location_id','Merchant_id'],how='left')
# ugbtrain = pd.merge(ugbtrain,kbtrainsubcol,on=['Location_id','Merchant_id'],how='left')
# ugbtrain['day_avtive_user'] = ugbtrain['ucnt']/ugbtrain['m_exist_days']
# ugbtrain['unt_cnt_ratio'] = ugbtrain['ucnt']/ugbtrain['cnt']
# ugbtrain.to_csv('kbtrain.csv',index=False)

# #kb test集
# ugbtest = kbtest.groupby(['Merchant_id','Location_id','User_id']).size()
# ugbtest = ugbtest.reset_index()
# ugbtest.rename(columns={0:'cnt'},inplace=True)
# ugbtest['ucnt'] = 1
# ugbtest = ugbtest.groupby(['Merchant_id','Location_id']).agg({'cnt':np.sum,'ucnt':np.sum})
# ugbtest = ugbtest.reset_index()

# ugbtest3 = kbtest[kbtest['ts']<='2015-11-26'].groupby(['Merchant_id','Location_id','User_id']).size()
# ugbtest3 = ugbtest3.reset_index()
# ugbtest3.rename(columns={0:'3cnt'},inplace=True)
# ugbtest3['3ucnt'] = 1
# ugbtest3 = ugbtest3.groupby(['Merchant_id','Location_id']).agg({'3cnt':np.sum,'3ucnt':np.sum})
# ugbtest3 = ugbtest3.reset_index()


# ugbtest7 = kbtest[kbtest['ts']<='2015-11-23'].groupby(['Merchant_id','Location_id','User_id']).size()
# ugbtest7 = ugbtest7.reset_index()
# ugbtest7.rename(columns={0:'7cnt'},inplace=True)
# ugbtest7['7ucnt'] = 1
# ugbtest7 = ugbtest7.groupby(['Merchant_id','Location_id']).agg({'7cnt':np.sum,'7ucnt':np.sum})
# ugbtest7 = ugbtest7.reset_index()

# ugbtest15 = kbtest[kbtest['ts']<='2015-11-15'].groupby(['Merchant_id','Location_id','User_id']).size()
# ugbtest15 = ugbtest15.reset_index()
# ugbtest15.rename(columns={0:'15cnt'},inplace=True)
# ugbtest15['15ucnt'] = 1
# ugbtest15 = ugbtest15.groupby(['Merchant_id','Location_id']).agg({'15cnt':np.sum,'15ucnt':np.sum})
# ugbtest15 = ugbtest15.reset_index()

# ugbtest = pd.merge(ugbtest,ugbtest3,on=['Merchant_id','Location_id'],how='left')
# ugbtest = pd.merge(ugbtest,ugbtest7,on=['Merchant_id','Location_id'],how='left')
# ugbtest = pd.merge(ugbtest,ugbtest15,on=['Merchant_id','Location_id'],how='left')

# ugbtest = pd.merge(ugbtest,mrt,on=['Merchant_id','Location_id'],how='left')
# ugbtest = ugbtest.fillna(0)
# ugbtest['bgt'] = ugbtest['Budget']/100
# ugbtest['bgt'] = ugbtest['bgt'].apply(lambda x:round(x))

# ugbtest_bgt_gb = ugbtest.groupby(['bgt']).agg({'cnt':np.mean,'ucnt':np.mean})
# ugbtest_bgt_gb = ugbtest_bgt_gb.reset_index()
# ugbtest_bgt_gb.rename(columns={'cnt':'bcntmean','ucnt':'bucntmean'},inplace=True)
# ugbtest = pd.merge(ugbtest,ugbtest_bgt_gb,on=['bgt'],how='left')

# kbtestsubcol = kbtest.groupby(['Location_id','Merchant_id','ts']).agg({'m_exist_days':'max'})
# kbtestsubcol = kbtestsubcol.reset_index()
# kbtestsubcol['buyday'] = 1
# kbtestsubcol = kbtestsubcol.groupby(['Location_id','Merchant_id']).agg({'m_exist_days':'max','buyday':np.sum})
# kbtestsubcol = kbtestsubcol.reset_index()
# kbtestsubcol['nobuyday'] = kbtestsubcol['m_exist_days']-kbtestsubcol['buyday']
# kbtestsubcol['buycntratio'] = kbtestsubcol['buyday']/kbtestsubcol['m_exist_days']


# mlcount = kbtest.groupby(['Location_id','Merchant_id','ts']).size()
# mlcount = mlcount.reset_index()
# mlcount.rename(columns={0:'sizemax'},inplace=True)
# mlcount['sizemin'] = mlcount['sizemax']
# mlcount['sizemean'] = mlcount['sizemax']
# mlcount['sizemed'] = mlcount['sizemax']
# mlcount['sizestd'] = mlcount['sizemax']
# mlcountgb = mlcount.groupby(['Location_id','Merchant_id']).agg({'sizemax':'max','sizemin':'min','sizemean':np.mean,'sizemed':np.median,'sizestd':np.std})
# mlcountgb = mlcountgb.reset_index()

# kbtestsubcol = pd.merge(kbtestsubcol,mlcountgb,on=['Location_id','Merchant_id'],how='left')
# ugbtest = pd.merge(ugbtest,kbtestsubcol,on=['Location_id','Merchant_id'],how='left')
# ugbtest['day_avtive_user'] = ugbtest['ucnt']/ugbtest['m_exist_days']
# ugbtest['unt_cnt_ratio'] = ugbtest['ucnt']/ugbtest['cnt']
# ugbtest.to_csv('kbtest.csv',index=False)
