#!/usr/bin/python
# coding=utf-8
from __future__ import division
import pandas as pd
import numpy as np
import xgboost as xgb
import pywFM
from sklearn.feature_extraction import DictVectorizer
from sklearn.externals import joblib
from sklearn.preprocessing import normalize

def gbdt_train(dftrain,dftest,dense_feature,target,param,num_round):

	watchlist  = [(xgb.DMatrix(dftest[dense_feature], dftest[target]),'eval'),
	(xgb.DMatrix(dftrain[dense_feature], dftrain[target]),'train')]

	bst = xgb.train(param, xgb.DMatrix(dftrain[dense_feature], dftrain[target]), num_round, watchlist)
	return bst

def get_gbdt_lindex(model,df,dense_feature):
	result = model.predict(xgb.DMatrix(df[dense_feature]),pred_leaf=True)
	rdf = pd.DataFrame(result)
	return rdf

##谨慎选取onehot特征，不要过多
def onehotfmdf(df,onehots,dv = None):
	for col in onehots:
		df[col] = df[col].apply(lambda x:str(x))
	if dv is None:
		vectorizer = DictVectorizer( sparse = True)
		oh_array = vectorizer.fit_transform(df.to_dict('records'))
		#oh_array = normalize(oh_array, norm='l1', axis=0)
		return oh_array,vectorizer
	else:
		oh_array = dv.transform(df.to_dict('records'))
		#oh_array = normalize(oh_array, norm='l1', axis=0)
		return oh_array


def libfm(tr_x,tr_y,te_x,te_y):
	fm = pywFM.FM(task='classification', num_iter=50,r2_regularization=0.01,learning_method='sgd',k2=3)
	model = fm.run(tr_x, tr_y, te_x, te_y)
	return model


def nomolized(df,cols):
	for col in cols:
		df[col] = (df[col]-df[col].mean())/(df[col].max()-df[col].min()+0.000001)
	return df

def nomolizedA(df,cols):
	for col in cols:
		df[col] = (df[col]-df[col].mean())/(df[col].std()+0.0000001)
	return df


if __name__ == '__main__':
	path = '/Users/simon/Downloads/tianchi/'
	train = pd.read_csv(path+'middata/kbtrain.csv')
	cv = pd.read_csv(path+'middata/cvtest.csv')
	gbpvt = pd.read_csv(path+'middata/gbpvt.csv')
	gbpvt = nomolizedA(gbpvt,gbpvt.columns[1:])
	utb2 = pd.read_csv(path+'middata/train_tb2.csv')
	utb2 = nomolizedA(utb2,list(set(utb2.columns)^set(['User_id'])))
	#utb2 = utb2[['User_id','buyratio','buycnt','cnt']]
	del train['mstartday']
	del cv['mstartday']
	print "load train data over"
	print utb2.head()

	onehots = ['User_id','Location_id','Merchant_id']
	target = 'label'
	

	train0 = train[train['label']==0].sample(frac=0.4)
	train1 = train[train['label']==1]
	train = train0.append(train1)
	train = train.append(train1)
	print train.head()
	#####乱序训练集，避免fm优化问题#####
	train = train.iloc[np.random.permutation(len(train))]
	print train.head()
	#####交叉验证获得gbdt模型###########
	train = pd.merge(train,utb2,on=['User_id'],how='inner')
	train = train.dropna()
	train0 = train[train['label']==0]
	train1 = train[train['label']==1]
	cv = pd.merge(cv,utb2,on=['User_id'],how='inner')
	cv = cv.dropna()

	dfeature = list(set(train.columns)^set(onehots))
	dfeature = list(set(dfeature)^set([target]))

	print "xgboost begin"
	params = {"objective": "binary:logistic",
          "eta": 0.1,
          "max_depth": 6,
          "min_child_weight": 3,
          "subsample":0.8,
          "colsample_bytree":0.8,
          "booster":"gbtree",
          "scale_pos_weight":train0.shape[0]/(train1.shape[0]*2)
	}
	num_round = 6
	gbdt_model = gbdt_train(train,cv,dfeature,'label',params,num_round)
	print "xgboost end"

    ####得到训练集的叶子节点
	tr_lf_index = get_gbdt_lindex(gbdt_model,train,dfeature)
	cv_lf_index = get_gbdt_lindex(gbdt_model,cv,dfeature)
    #####合并gbdt叶节点特征和one-hot特征
	# tr_lf_index[onehots] = train[onehots]
	# cv_lf_index[onehots] = cv[onehots]
	tr_lf_index['User_id'] = train['User_id']
	cv_lf_index['User_id'] = train['User_id']

	########################################使用fm训练######################################
	#####开始one-hot并且归一化###
	print "tr_lf_index begin"
	tr_lf_index = pd.merge(tr_lf_index,gbpvt,on='User_id',how='left')
	cv_lf_index = pd.merge(cv_lf_index,gbpvt,on='User_id',how='left')
	del tr_lf_index['User_id']
	del cv_lf_index['User_id']
	print tr_lf_index.head()
	print cv_lf_index.head()
	print tr_lf_index.columns[:num_round]
	# lastcols = sorted(list(set(tr_lf_index.columns)^set(['User_id'])))
	# tr_lf_index_part = tr_lf_index[:num_round]
	# cv_lf_index_part = cv_lf_index[:num_round]
	# tr_lf_index = tr_lf_index.head(100)
	# cv_lf_index = cv_lf_index.head(100)
	tr_ohdf,dvfm = onehotfmdf(tr_lf_index,tr_lf_index.columns[:num_round])
	#cv_ohdf = onehotfmdf(cv_lf_index,cv_lf_index.columns[:num_round],dvfm)
	print "tr_lf_index end"
	####开始训练fm样本
	
	######生成预测集#######
	test = pd.read_csv(path+'middata/kbtest.csv')
	test = pd.merge(test,utb2,on=['User_id'],how='inner')
	test = test.dropna()
	#test = nomolized(test,dfeature)
	test['label'] = 0
	del test['mstartday']
	te_lf_index = get_gbdt_lindex(gbdt_model,test,dfeature)
	#te_lf_index[onehots] = test[onehots]
	te_lf_index['User_id'] = test['User_id']
	te_lf_index = pd.merge(te_lf_index,gbpvt,on='User_id',how='left')
	del te_lf_index['User_id']
	#te_lf_index_part = te_lf_index[:num_round]
	#te_lf_index = te_lf_index.head(100)
	te_ohdf = onehotfmdf(te_lf_index,te_lf_index.columns[:num_round],dvfm)

	print "begin train fm_model"
	#fm_model = libfm(tr_ohdf,train['label'],cv_ohdf,cv['label'],te_ohdf,test['label'])
	######看看参数怎么样
	# tr_ohdf['label'] = train['label'].head(100)
	# tr_ohdf.to_csv(path+'middata/tr_ohdf.csv')
	# te_ohdf['label'] = 0
	# te_ohdf.to_csv(path+'middata/te_ohdf.csv')
	# cv_ohdf['label'] = cv['label'].head(100)
	# cv_ohdf.to_csv(path+'middata/cv_ohdf.csv')

	fm_model = libfm(tr_ohdf,train['label'],te_ohdf,test['label'])
	print "end train fm_model"
	#result = fm_model.predict(te_ohdf)
	test['result'] = fm_model.predictions
	test = test[['User_id','Location_id','Merchant_id','result']]
	#test.to_csv(path+'middata/fm_result.csv',index=False)

	ulgb = test.groupby(['User_id','Location_id']).agg({'result':'max'}).reset_index()
	ulgb = pd.merge(ulgb,test,on=['User_id','Location_id','result'],how='inner')
	ulgb.to_csv(path+'result/yjh.csv',index=False)

	#######################################使用ffm训练###################################
	list1 = {}
	list2 = {}
	r = {}
	for i in range(1,73):
		list1[i] = u1[str(i)].describe()[1]
		list2[i] = u2[str(i)].describe()[1]
		std = gbpvt[str(i)].describe()[2]
		r[i] = (list2[i]-list1[i])/std




