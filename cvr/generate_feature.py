from __future__ import division
import pandas as pd
import numpy as np
import datetime
import math
import os
import commands
import xgboost as xgb
import pywFM
from bayessmoth import HyperParam
from sklearn.feature_extraction import DictVectorizer
from sklearn.externals import joblib
from sklearn.preprocessing import normalize

def ELF_hash(code):
    hashcode = 0
    x = 0
    strcode = str(code)
    for i, ch in enumerate(strcode):
        hashcode = (hashcode << 4) + ord(ch)
        x = hashcode & 0xF0000000
        if x != 0:
            hashcode ^= (x >> 24)
        hashcode &= ~x

    return (hashcode & 0x7FFFFFFF) % hashsize

# def getCVR(df,cols,target):
# 	for col in cols:
# 		gb = df.groupby([col,target]).size().reset_index()
# 		gb_pvt = gb.pivot_table(0,[col],target).reset_index()
# 		gb_pvt = gb_pvt.fillna(0)
# 		key = col+'cvr'
# 		gb_pvt[key] = gb_pvt[1]/(gb_pvt[0]+gb_pvt[1])
# 		cvrlist[key] = gb_pvt

def onehotfmdf(train,test,onehots,target,nouse=None):
	columns = []
	if nouse is not None:
		columns = list(set(train.columns)^set(nouse))
		columns = list(set(columns)^set([target]))
	else:
		columns = list(set(train.columns)^set([target]))

	print columns

	train_size = train.shape[0]
	test_size = test.shape[0]
	total = train[columns].append(test[columns])
	print total.shape,train[columns].shape,test[columns].shape
	onehot_size = 1
	onehot_map = {}
	for i in range(0,len(onehots)):
		onehot_map[onehots[i]] = (i+1)
	print onehot_map

	print "begin loop"
	for col in columns:
		if col in onehot_map.keys():
			####one-hot###
			total[col] = (pd.factorize(total[col])[0]+onehot_size)
			colmax = total[col].max()
			print col,colmax
			total[col] = total[col].apply(lambda x:'%s:%s'%(str(x),"1"))
			onehot_size = colmax+1
		else:
			colcnt = onehot_size
			print "not onehot",col,colcnt
			total[col] = total[col].apply(lambda x:'%s:%s'%(str(colcnt),str(x)))
			onehot_size = onehot_size+1

	starget = train[target].append(test[target])
	total.insert(0,target,starget)
	total.head(train_size).to_csv('fmtrain',sep=' ',header=False,index=False)
	total.tail(test_size).to_csv('fmtest',sep=' ',header=False,index=False)


def onehotffmdf(train,test,onehots,target,nouse=None):
	columns = []
	if nouse is not None:
		columns = list(set(train.columns)^set(nouse))
		columns = list(set(columns)^set([target]))
	else:
		columns = list(set(train.columns)^set([target]))

	print columns

	train_size = train.shape[0]
	test_size = test.shape[0]
	total = train[columns].append(test[columns])
	print total.shape,train[columns].shape,test[columns].shape
	onehot_size = 1
	onehot_map = {}
	for i in range(0,len(onehots)):
		onehot_map[onehots[i]] = (i+1)
	print onehot_map

	print "begin loop"
	for col in columns:
		if col in onehot_map.keys():
			####one-hot###
			total[col] = (pd.factorize(total[col])[0]+onehot_size)
			colmax = total[col].max()
			field = onehot_map[col]
			print field,col,colmax
			total[col] = total[col].apply(lambda x:'%s:%s:%s'%(str(field),str(x),"1"))
			onehot_size = colmax+1
		else:
			field = len(onehots)+1
			colcnt = onehot_size
			print "not onehot",col,field,colcnt
			total[col] = total[col].apply(lambda x:'%s:%s:%s'%(str(field),str(colcnt),str(x)))
			onehot_size = onehot_size+1

	starget = train[target].append(test[target])
	total.insert(0,target,starget)
	total.head(train_size).to_csv('ffmtrain',sep=' ',header=False,index=False)
	total.tail(test_size).to_csv('ffmtest',sep=' ',header=False,index=False)

def hashffm(df,onehots,target,filename,nouse=None):
	columns = []
	if nouse is not None:
		columns = list(set(df.columns)^set(nouse))
		columns = list(set(columns)^set([target]))
	else:
		columns = list(set(df.columns)^set([target]))

	tmpdf = df[columns]
	print columns


	onehot_map = {}

	for i in range(0,len(onehots)):
		onehot_map[onehots[i]] = (i+1)
	densefield = len(onehot_map)+1

	for col in columns:
		if col in onehot_map.keys():
			field = onehot_map[col]
			tmpdf[col] = tmpdf[col].apply(lambda x:'%s:%s:%s'%(str(field),ELF_hash(col+str(x)),'1'))
		else:			
			tmpdf[col] = tmpdf[col].apply(lambda x:'%s:%s:%s'%(str(densefield),ELF_hash(col),str(x)))

	tmpdf.insert(0,target,df[target])
	tmpdf.to_csv(filename,sep=' ',header=False,index=False)

def nomolizedA(df,cols):
	for col in cols:
		mean = df[col].mean()
		std = df[col].std()
		df[col] = (df[col]-mean)/(std+0.0000001)
	return df

def nomolized(df,cols):
	for col in cols:
		df[col] = (df[col]-df[col].min())/(df[col].max()-df[col].min()+0.000001)
	return df

def hashfm(df,onehots,target,filename,nouse=None):
	columns = []
	if nouse is not None:
		columns = list(set(df.columns)^set(nouse))
		columns = list(set(columns)^set([target]))
	else:
		columns = list(set(df.columns)^set([target]))

	tmpdf = df[columns]
	print columns

	onehot_map = {}
	for i in range(0,len(onehots)):
		onehot_map[onehots[i]] = (i+1)

	for col in columns:
		if col in onehot_map.keys():
			tmpdf[col] = tmpdf[col].apply(lambda x:'%s:%s'%(ELF_hash(str(col)+str(x)),'1'))
		else:
			tmpdf[col] = tmpdf[col].apply(lambda x:'%s:%s'%(ELF_hash(str(col)),str(x)))

	tmpdf.insert(0,target,df[target])
	tmpdf.to_csv(filename,sep=' ',header=False,index=False)


def gbdt_train(dftrain,dftest,dense_feature,target,param,num_round):

	watchlist  = [(xgb.DMatrix(dftest[dense_feature], dftest[target]),'eval'),
	(xgb.DMatrix(dftrain[dense_feature], dftrain[target]),'train')]

	bst = xgb.train(param, xgb.DMatrix(dftrain[dense_feature], dftrain[target]), num_round, watchlist)
	return bst

def get_gbdt_lindex(model,df,dense_feature):
	result = model.predict(xgb.DMatrix(df[dense_feature]),pred_leaf=True)
	rdf = pd.DataFrame(result)
	return rdf

def get_gbdt_preds(model,df,dense_feature):
	result = model.predict(xgb.DMatrix(df[dense_feature]))
	rdf = pd.DataFrame(result)
	return rdf

cvrlist = {}
def getCVR(df,cols,target):
	for col in cols:
		gb = df.groupby([col,target]).size().reset_index()
		gb_pvt = gb.pivot_table(0,[col],target).reset_index()
		gb_pvt = gb_pvt.fillna(0)
		key = 'cvr'+col
		#cvrlist[key] = gb_pvt
		gb_pvt[1] = gb_pvt[1]+gb_pvt[1].describe()[5]
		gb_pvt[0] = gb_pvt[0]+gb_pvt[0].describe()[5]

		gb_pvt.rename(columns={0:'noconv'+col},inplace=True)
		gb_pvt.rename(columns={1:'conv'+col},inplace=True)
		gb_pvt[key] = gb_pvt['conv'+col]/(gb_pvt['noconv'+col]+gb_pvt['conv'+col])
		df = pd.merge(df,gb_pvt[[col,key]],on=[col],how='left')
	return df

def get_dist_cvr(df,cols,stdcols,target):
	for col in cols:
		stdcols_gb = df.groupby(stdcols).size().reset_index()
		del stdcols_gb[0]
		stdcols_gb = stdcols_gb.reset_index()
		stdcols_gb.rename(columns={'index':'idx'},inplace=True)

		allcol = list(set([col])|set(stdcols))
		allcol_target = list(set(allcol)|set([target]))
		gb = df.groupby(allcol_target).size().reset_index()
		gb.rename(columns={0:'gbcnt'},inplace=True)
		merge = pd.merge(stdcols_gb,gb,on=stdcols,how='left')

		merge_col = list(set(allcol)|set(['idx']))
		merge = merge.pivot_table('gbcnt',merge_col,target).reset_index()
		merge = merge.fillna(0)
		merge['total'] = merge[0]+merge[1]
		for idxs in sorted(merge['idx'].unique()):
			merge_tmp = merge[merge['idx']==idxs]
			hyper = HyperParam(1,1)
			hyper.update_from_data(merge_tmp['total'].tolist(),merge_tmp[1].tolist())
			if np.isnan(hyper.alpha) or np.isnan(hyper.beta):
				hyper.alpha = 0
				hyper.beta = 0
			merge_tmp['cvr'+col] = (merge_tmp[1]+hyper.alpha)/(merge_tmp['total']+hyper.beta+hyper.alpha)
			print 'cvr'+col,hyper.beta,hyper.alpha
			merge_tmp = merge_tmp[list(set(allcol)|set(['cvr'+col]))]
			for index, row in merge_tmp.iterrows():
				cvr = merge_tmp.loc[index,'cvr'+col]
				merge.loc[index,'cvr'+col] = cvr

			#merge = pd.merge(merge,merge_tmp,left_index=True,right_index=True,how='left')
		print allcol
		merge = merge[list(set(allcol)|set(['cvr'+col]))]
		df = pd.merge(df,merge,on=allcol,how='left')
	return df

		

def df_deal(df):
	#del df['s_model']
	df['conv'] = np.where(df['adtoken.1'].isnull(),0,1)
	del df['adtoken.1']
	del df['adtoken']
	print df.shape
	df = df.dropna()
	df = df.reset_index()
	del df['index']
	df['subsys'] = df['systemversion'].apply(lambda x:str(x)[0])
	print df.shape
	return df

if __name__ == '__main__':
	path = '/work/lsmdata/jstreemodel/dmp/'
	os.environ['path']=path
	cvrlist = commands.getoutput('ls $path |grep cvr.tsv')
	cvrlist = cvrlist.split('\n')
	train = pd.DataFrame()
	for file in cvrlist:
		tmp = pd.read_table(path+file)
		print tmp.shape
		train = train.append(tmp)

	cv = pd.DataFrame()
	for i in range(20160401,20160410):
		tmp = pd.read_table('/work/lsmdata/jstreemodel/%s_cvr.tsv'%i)
		cv = cv.append(tmp)

	####################以上为数据导入###################################################

	train = df_deal(train)
	cv = df_deal(cv)

	target = 'conv'
	nouse = ['uid','creative_unit_id','adgroup_id','systemversion']
	onehots_param = []
	#######产生dense feature########
	train = get_dist_cvr(train,['width','height','appcategoryid','isjailbroken','displaytype','devicetype','req_province_id','creativeid','campaign_id','chour','subsys','appid'],['platform', 'sspid', 'networkstate'],'conv')

	cv = get_dist_cvr(cv,['width','height','appcategoryid','isjailbroken','displaytype','devicetype','req_province_id','creativeid','campaign_id','chour','subsys','appid'],['platform', 'sspid', 'networkstate'],'conv')


	# train['conv'] = np.where(train['adtoken.1'].isnull(),0,1)
	# cv['conv'] = np.where(cv['adtoken.1'].isnull(),0,1)
	# del train['adtoken.1']
	# del cv['adtoken.1']
	# del train['adtoken']
	# del cv['adtoken']
	# train = train.dropna()
	# train = train.reset_index()
	# cv = cv.dropna()
	# cv = cv.reset_index()
	# del train['index']
	# del cv['index']

	# train['subsys'] = train['systemversion'].apply(lambda x:str(x)[0])
	# cv['subsys'] = cv['systemversion'].apply(lambda x:str(x)[0])

	
	# #####负采样####
	# train0 = train[train[target]==0].sample(frac=0.05)
	# train1 = train[train[target]==1]
	# tall = train0.append(train1)
	tall = train
	######shuffle,对于sgb这种训练，必须随机排序样本
	tall = tall.iloc[np.random.permutation(len(tall))]
	####shuffle后，续重新设置索引
	tall = tall.reset_index()
	del tall['index']

	for col in nouse:
		del tall[col]
		del cv[col]

	

	dense_feature = []
	for col in tall.columns:
		if col.startswith('cvr'):
			dense_feature.append(col)

	param_tree = {"objective": "binary:logistic",
          "eta": 1,
          "max_depth": 7,
          "min_child_weight": 3,
          "subsample":0.8,
          "colsample_bytree":1,
          "booster":"gbtree",
          "eval_metric":"error",
          "scale_pos_weight":tall[tall[target]==0].shape[0]/tall[tall[target]==1].shape[0]
	}
	num_round_tree = 5
	model_tree = gbdt_train(tall,cv,dense_feature,'conv',param_tree,num_round_tree)

	param_liner = {"objective": "binary:logistic",
		  "eta": 0.3,
          "booster ":"gblinear",
          "lambda":1,
          "eval_metric":"error",
          "scale_pos_weight":tall[tall[target]==0].shape[0]/tall[tall[target]==1].shape[0]
	}
	num_round_linear = 8
	model_linear = gbdt_train(tall,cv,dense_feature,'conv',param_liner,num_round_linear)

	tall_gbtleave = get_gbdt_lindex(model_tree,tall,dense_feature)
	tall_gblleave = get_gbdt_lindex(model_linear,tall,dense_feature)
	tall_gblleave = tall_gblleave.add_prefix('line')
	cv_gbtleave = get_gbdt_lindex(model_tree,cv,dense_feature)
	cv_gblleave = get_gbdt_lindex(model_linear,cv,dense_feature)
	cv_gblleave = cv_gblleave.add_prefix('line')

	cvtreepred = get_gbdt_preds(model_tree,cv,dense_feature)
	cvlinepred = get_gbdt_preds(model_linear,cv,dense_feature)
	preds = (cvtreepred+cvlinepred)/2

	talltmp = pd.concat([tall,tall_gbtleave],axis=1)
	talltmp = pd.concat([talltmp,tall_gblleave],axis=1)
	cvtmp = pd.concat([cv,cv_gbtleave],axis=1)
	cvtmp = pd.concat([cvtmp,cv_gblleave],axis=1)
	######fm 算法训练和测试数据集
	talln = nomolizedA(talltmp,dense_feature)
	cvn = nomolizedA(cvtmp,dense_feature)

	for col in talln.columns:
		if not str(col).startswith('cvr'):
			onehots_param.append(col)

	onehotfmdf(talln,cvn,onehots_param,target)

	# hashfm(talln,onehots_param,target,'tall525.fm')
	# hashfm(cvn,onehots_param,target,'cv525.fm')


	/root/lsm/libfm/bin/libFM -task c -train dmp/tallhead.fm -test dmp/tallhead.fm -dim '1,1,6' -cache_size 100000000 -iter 300 -method mcmc -verbosity 1 -init_stdev 0.5



	######ffm算法训练和测试数据集,样本量不够的时候，效果很差
	# hashffm(tall,tall.columns,target,'tall.ffm')
	# hashffm(cv,cv.columns,target,'cv.ffm')

















	
