from math import log
from __future__ import division
import numpy as np
import pandas as pd
from scipy import special
import pickle
import json
from scipy.sparse import csr_matrix
import xgboost as xgb
import os
import commands

folder = '/work/lsmdata/jstreemodel/dmp'
os.environ['folder']=folder
cvrlist = commands.getoutput('ls $folder |grep cvr.tsv')
cvrlist = cvrlist.split('\n')

####读取所有cvr df
cvrdf = pd.DataFrame()
for file in cvrlist:
       tmp = pd.read_table(file)
       cvrdf = cvrdf.append(tmp)
###read 完毕

cvrdf = cvrdf[(cvrdf['appid'].notnull())&(cvrdf['dspid']==1)]
cvrdf['subsys'] = cvrdf['systemversion'].apply(lambda x:str(x)[0])
del cvrdf['adtoken']
cvrdf.rename(columns={'adtoken.1':'conv'},inplace=True)

cvrlist = {}
def getCVR(df,cols,target):
       for col in cols:
              gb = df.groupby([col,target]).size().reset_index()
              gb_pvt = gb.pivot_table(0,[col],target).reset_index()
              gb_pvt = gb_pvt.fillna(0)
              key = col+'cvr'
              gb_pvt[key] = gb_pvt[1]/(gb_pvt[0]+gb_pvt[1])
              cvrlist[key] = gb_pvt


test_probs = (rf.predict_proba(test[features])[:,1] + gbm.predict(xgb.DMatrix(test[features])))/2