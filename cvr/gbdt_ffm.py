#!/usr/bin/python
# coding=utf-8
import pandas as pd
import numpy as np
import xgboost as xgb
import pywFM
from sklearn.feature_extraction import DictVectorizer
from sklearn.externals import joblib
from sklearn.preprocessing import normalize

def gbdt_train(dftrain,dftest,dense_feature,target,param,num_round):

	watchlist  = [(xgb.DMatrix(dftest[dense_feature], dftest[target]),'eval'),
	(xgb.DMatrix(dftrain[dense_feature], dftrain[target]),'train')]

	bst = xgb.train(param, xgb.DMatrix(dftrain[dense_feature], dftrain[target]), num_round, watchlist)
	return bst

def get_gbdt_lindex(model,df,dense_feature):
	result = model.predict(xgb.DMatrix(df[dense_feature]),pred_leaf=True)
	rdf = pd.DataFrame(result)
	return rdf

def to_ffm_file(df,onehots,target,filename):
        start = 0
        fo = open(filename, 'w')
        ###这里只处理one-hot特征，下面一个循环是所有特征
        for col in onehots:
    		df[col] = pd.factorize(df[col])[0]
    		df[col] = df[col]+start
    		start = df[col].max()

        for idx, row in df.iterrows():
        	field = 1
        	one_line = ''
        	for col in df.columns:
        		one_line = "%s %s:%s %s" %(one_line,str(field),row[col],'1')
        		if field < (len(onehots)+1):
        			field = field+1

        	fo.write(str(row[target]) + one_line + '\n')



# ##谨慎选取onehot特征，不要过多
# def onehotfmdf(df,onehots,dv = None):
#     for col in onehots:
#         df[col] = df[col].astype('str')
#     if dv is None:
#     	vectorizer = DictVectorizer( sparse = True)
#     	oh_array = vectorizer.fit_transform(df.to_dict('records'))
#     	#oh_array = normalize(oh_array, norm='l1', axis=0)
#     	return oh_array,vectorizer
#     else:
#         oh_array = dv.transform(df.to_dict('records'))
#         #oh_array = normalize(oh_array, norm='l1', axis=0)
#         return oh_array


# def onehotffmdf(df,onehots,dv = None):
# 	for col in onehots:
# 		df[col] = df[col].astype('str')
# 	if dv is None:
# 		vectorizer = DictVectorizer( sparse = True)
# 		oh_array = vectorizer.fit_transform(df.to_dict('records'))

# 		output = df.copy()
# 		for icol in range(0,len(df.columns)):
# 			tmp = pd.DataFrame(df.iloc[:,icol])
# 			col_array = vectorizer.transform(tmp.to_dict('records'))
# 			cx = col_array.tocoo()
# 			for i,j,v in itertools.izip(cx.row, cx.col, cx.data):
# 				k = (i,j,v)
# 				output.iloc[i,icol] = 'fieldxxx'+str(k[1])+':'+str(k[2])

# 		print output

# 		return vectorizer
# 	else:
# 		vectorizer = dv
# 		output = df.copy()
# 		for icol in range(0,len(df.columns)):
# 			col_array = vectorizer.transform(df[icol].to_dict('records'))
# 			cx = col_array.tocoo()
# 			for i,j,v in itertools.izip(cx.row, cx.col, cx.data):
# 				#(i,j,v)
# 				output.iloc[i,icol] = 'fieldxxx'+str(j)+':'+str(v)

# 		print output

# def onehotfmdf(train,test,onehots,target,nouse=None):
# 	columns = []
# 	if nouse is not None:
# 		columns = list(set(train.columns)^set(nouse))
# 		columns = list(set(columns)^set([target]))
# 	else:
# 		columns = list(set(train.columns)^set([target]))

# 	print columns

# 	train_size = train.shape[0]
# 	test_size = test.shape[0]
# 	total = train[columns].append(test[columns])
# 	print total.shape,train[columns].shape,test[columns].shape
# 	onehot_size = 1
# 	onehot_map = {}
# 	for i in range(0,len(onehots)):
# 		onehot_map[onehots[i]] = (i+1)
# 	print onehot_map

# 	print "begin loop"
# 	for col in columns:
# 		if col in onehot_map.keys():
# 			####one-hot###
# 			total[col] = (pd.factorize(total[col])[0]+onehot_size)
# 			colmax = total[col].max()
			

def onehotffmdf(train,test,onehots,target,nouse=None):
	columns = []
	if nouse is not None:
		columns = list(set(train.columns)^set(nouse))
		columns = list(set(columns)^set([target]))
	else:
		columns = list(set(train.columns)^set([target]))

	print columns

	train_size = train.shape[0]
	test_size = test.shape[0]
	total = train[columns].append(test[columns])
	print total.shape,train[columns].shape,test[columns].shape
	onehot_size = 1
	onehot_map = {}
	for i in range(0,len(onehots)):
		onehot_map[onehots[i]] = (i+1)
	print onehot_map

	print "begin loop"
	for col in columns:
		if col in onehot_map.keys():
			####one-hot###
			total[col] = (pd.factorize(total[col])[0]+onehot_size)
			colmax = total[col].max()
			field = onehot_map[col]
			print field,col,colmax
			total[col] = total[col].apply(lambda x:'%s:%s:%s'%(str(field),str(x),"1"))
			onehot_size = colmax+1
		else:
			field = len(onehots)+1
			colcnt = onehot_size
			print "not onehot",col,field,colcnt
			total[col] = total[col].apply(lambda x:'%s:%s:%s'%(str(field),str(colcnt),str(x)))
			onehot_size = onehot_size+1

	starget = train[target].append(test[target])
	total.insert(0,target,starget)
	total.head(train_size).to_csv('ffmtrain',sep=' ',header=False,index=False)
	total.tail(test_size).to_csv('ffmtest',sep=' ',header=False,index=False)






def libfm(tr_x,tr_y,te_x,te_y,cv_x,cv_y):
	fm = pywFM.FM(task='classification', num_iter=20,r2_regularization=0.01)
	model = fm.run(tr_x, tr_y, te_x, te_y,cv_x,cv_y)
	return model



def nomolized(df,cols):
	for col in cols:
		df[col] = (df[col]-df[col].min())/(df[col].max()-df[col].min()+0.000001)
	return df

if __name__ == '__main__':
	path = '/data/'
	train = pd.read_csv(path+'xunlianji')
	cv = pd.read_csv(path+'cvji')

	print train.head()
	print cv.head()

	onehots = ['User_id','Location_id','Merchant_id']
	target = 'label'
	dfeature = list(set(train.columns)^set(onehots))
	dfeature = list(set(dfeature)^set([target]))

	train0 = train[train['label']==0].sample(frac=0.3)
	train1 = train[train['label']==1]
	train = train0.append(train1)
	#####交叉验证获得gbdt模型
	train = pd.merge(train,utb2,on=['User_id'],how='inner')
	train = train.dropna()
	cv = pd.merge(cv,utb2,on=['User_id'],how='inner')
	cv = cv.dropna()
	train = nomolized(train,dfeature)
	cv = nomolized(cv,dfeature)
	gbpvt = nomolized(gbpvt,gbpvt.columns[1:])
	# train = normalize(train, norm='l1', axis=0)
	# cv = normalize(cv, norm='l1', axis=0)

	print "xgboost begin"
	params = {"objective": "binary:logistic",
          "eta": 0.1,
          "max_depth": 6,
          "min_child_weight": 3,
          "subsample":0.8,
          "colsample_bytree":0.7,
          "scale_pos_weight":train0.shape[0]/train1.shape[0]
	}
	num_round = 6
	gbdt_model = gbdt_train(train,cv,dfeature,'label',params,num_round)
	print "xgboost end"

    ####得到训练集的叶子节点
	tr_lf_index = get_gbdt_lindex(gbdt_model,train,dfeature)
	cv_lf_index = get_gbdt_lindex(gbdt_model,cv,dfeature)
    #####合并gbdt叶节点特征和one-hot特征
	train = train.reset_index()
	del train['index']
	tr_lf_index[onehots] = train[onehots]
	cv_lf_index[onehots] = cv[onehots]

	########################################使用fm训练######################################
	#####开始one-hot并且归一化###
	print "tr_lf_index begin"
	tr_lf_index = pd.merge(tr_lf_index,gbpvt,on='User_id',how='left')
	cv_lf_index = pd.merge(cv_lf_index,gbpvt,on='User_id',how='left')
	print tr_lf_index.head()
	print cv_lf_index.head()
	print tr_lf_index.columns[:num_round]
	tr_ohdf,dvfm = onehotfmdf(tr_lf_index,tr_lf_index.columns[:num_round])
	cv_ohdf = onehotfmdf(cv_lf_index,cv_lf_index.columns[:num_round],dvfm)
	print "tr_lf_index end"
	####开始训练fm样本
	
	######生成预测集#######
	test = pd.read_csv(path+'middata/kbtest.csv')
	test = pd.merge(test,utb2,on=['User_id'],how='inner')
	test = test.dropna()
	test = nomolized(test,dfeature)
	test['label'] = 0
	del test['mstartday']
	te_lf_index = get_gbdt_lindex(gbdt_model,test,dfeature)
	te_lf_index[onehots] = test[onehots]
	te_ohdf = onehotfmdf(te_lf_index,te_lf_index.columns,dvfm)

	print "begin train fm_model"
	fm_model = libfm(tr_ohdf,train['label'],cv_ohdf,cv['label'],te_ohdf,test['label'])
	######看看参数怎么样
	fm_model = libfm(tr_ohdf,train['label'],te_ohdf,test['label'],cv_ohdf,cv['label'])
	print "end train fm_model"
	#result = fm_model.predict(te_ohdf)
	test['result'] = fm_model.predictions
	test = test[['User_id','Location_id','Merchant_id','result']]
	#test.to_csv(path+'middata/fm_result.csv',index=False)

	ulgb = test.groupby(['User_id','Location_id']).agg({'result':'max'})
	ulgb = pd.merge(ulgb,test,on=['User_id','Location_id','result'],how='inner')
	ulgb.to_csv(path+'result/yjh.csv',index=False)

	#######################################使用ffm训练###################################
