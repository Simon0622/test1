#!/usr/bin/python
# coding=utf-8

import scipy
import  sympy
import  math
import scipy.special as special
import numpy
import random


class HyperParam(object):
    def __init__(self, alpha, beta):
        self.alpha = alpha
        self.beta = beta

    def update_from_data(self, tries, success):

        """ 根据观察到的数据,计算后验分布的参数,也就是beta分布的,alpha 和beta
        data: set of array 输入的展示和点击量数据
        return: no
        """
        i = 0
        for i in range(5000):

            # self.alpha = self.updateAlpha(
            # data, self.alpha, self.beta)
            # self.beta = self.updateBeta(
            # data, self.alpha, self.beta)
            new_alpha, new_beta = self.__update_alpha_beta(tries, success, self.alpha, self.beta)
            if abs(new_alpha - self.alpha) < 0.00000001 and abs(new_beta - self.beta) < 0.00000001:
                break

            self.alpha = new_alpha
            self.beta = new_beta

    def update_from_data_by_em(self, tries, sucess):

        """用EM算法计算alpha beta
            data: array 输入的展示和点击量数据
            return: no
            """
        for i in range(3000):
            t, s = self.__e_step(tries, sucess)
            #print t, s, self.alpha, self.beta, i
            newalpha, newbeta = self.__m_step(t, s)
            if abs(self.alpha - newalpha) < 0.00000001 and abs(self.beta - newbeta) < 0.00000001:
                self.alpha, self.beta = newalpha, newbeta
                break
            self.alpha, self.beta = newalpha, newbeta
            # new alpha ,beta should make the log likelyhood of the data increase every time
            #self.__data_loglikelyhood(data)

    def __data_loglikelyhood(self, data):

        like_hood = 0.0
        for item in data:
            test = special.gammaln(item[0] + self.alpha + self.beta)
            like_hood += (special.gammaln(self.alpha + self.beta)) + (special.gammaln(item[1] + self.alpha)\
                       + special.gammaln(item[0] - item[1] + self.beta) - special.gammaln(item[0] + self.alpha + self.beta)\
                       - special.gammaln(self.alpha) - special.gammaln(self.beta))

        print "datalikehood:", like_hood

    def __e_step(self, tries, success):

        """e step"""
        t = 0.0
        s = 0.0

        for i in range(len(tries)):
            abn = special.digamma(self.alpha + self.beta + tries[i])
            t += (special.digamma(self.alpha + success[i]) - abn)
            s += (special.digamma(self.beta + tries[i] - success[i]) - abn)

        return t/len(tries), s/len(tries)

    def __m_step(self, t, s):

        old_alpha = self.alpha
        old_beta = self.beta

        afenzi = (special.digamma(old_alpha) - special.digamma(old_alpha + old_beta) - t)
        afenmu = (special.polygamma(1, old_alpha) - special.polygamma(1, old_alpha + old_beta))

        new_alpha = old_alpha - afenzi / afenmu

        if new_alpha <= 0.0:
           new_alpha = old_alpha

        bfenzi = (special.digamma(old_beta) - special.digamma(new_alpha + old_beta) - s)
        bfenmu = (special.polygamma(1, old_beta) - special.polygamma(1, new_alpha + old_beta))

        new_beta = old_beta - bfenzi / bfenmu
        if new_beta <=0.0:
            new_beta = old_beta

        return new_alpha, new_beta


    def update_from_data_by_moment(self, tries, success):

        """直接用矩估计的方法计算alpha beta
        data: array 输入的展示和点击量数据
        return: no
        """

        mean, var = self.compute_moment(tries, success)
        print '均值和方差', mean, var
        self.alpha = mean * (mean * (1 - mean) / (var+0.000001) - 1)
        self.beta = (1 - mean) * (mean * (1 - mean) / (var+0.000001) - 1)
    def compute_moment(self, tries, success):

        """计算数据的均值方差
        data: set of array,输入的展示和点击量数据
        return:均值,方差
        """

        ctr = 0.0
        var = 0.0
        I = 0.0
        C = 0.0
        for i in range(len(tries)):
            I += tries[i]
            C += success[i]
        if C == 0:
            print 'total conv == 0..................'
        mean = C/I
        for i in range(len(tries)):
            var += pow((float(success[i]) / float(tries[i])) - mean, 2)

        return mean, var / (len(tries) - 1)

    def __update_alpha_beta(self, tries, success, alpha, beta):

        """根据不动点迭代方程计算alpha,beta
            data: set of array,输入的展示量和点击量数据
            return: alpha,beta
            """

        sumfenzialpha = 0.0
        sumfenzibeta = 0.0
        sumfenmu = 0.0
        for i in range(len(tries)):
            sumfenzialpha += (special.digamma(success[i] + alpha) - special.digamma(alpha))
            sumfenzibeta += (special.digamma(tries[i] - success[i] + beta) - special.digamma(beta))

            sumfenmu += (special.digamma(tries[i] + alpha + beta) - special.digamma(alpha + beta))
        return alpha * (sumfenzialpha / sumfenmu), beta * (sumfenzibeta / sumfenmu)

    def __update_alpha_beta_2(self, data, alpha, beta):
        """根据不动点迭代方程计算alpha,beta
            data: set of array,输入的展示量和点击量数据
            return: alpha,beta
            """

        sumfenzialpha = 0.0
        sumfenzibeta = 0.0
        sumfenmu = 0.0
        for item in data:
            sumfenzialpha += item[1]/(item[1] - 1 + alpha)
            sumfenzibeta += (item[0]-item[1])/(item[0] - item[1] - 1 + beta)

            sumfenmu += item[0]/(item[0] - 1 + alpha + beta)
        return alpha * (sumfenzialpha / sumfenmu), beta * (sumfenzibeta / sumfenmu)
    def sample_from_beta(self, alpha, beta):

        """从beta分布中采样展示量和点击量,用于测试
            alpha: float beta分布的参数
            beta:  float beta分布的参数
            return: set of array 展示量和点击量的数组
            """

        sample = numpy.random.beta(alpha, beta, 10000)

        I = []
        C = []

        for click_rate in sample:
            imp = random.random() * 100000.0

            click = imp * click_rate

            I.append(imp)
            C.append(click)

        return I, C


def test():
    hyper = HyperParam(1, 1)

    data = [(1000.0, 115.0), (100, 4.0), (100.0, 10.0), (100.0, 12.0), (100, 10.0), (100.0, 1.0), (1000.0, 115.0),
            (10000, 19), (100000.0, 1000.0), (10000.0, 200.0), (1000, 30.0), (100.0, 4.0), (45, 1.0), (345.0, 1.0), (443.0, 2.0), (343, 3.0), (434.0, 4.0)]

    I, C = hyper.sample_from_beta(5, 1000.0)

    #hyper.update_from_data(data)

    #hyper.update_from_data_by_moment(data)
    #print hyper.alpha / (hyper.alpha + hyper.beta)

    #EM 算法
    #hyper.update_from_data_by_moment(data)
    hyper.update_from_data_by_em(I, C)


