#!/usr/bin/python
# coding=utf-8
from __future__ import division
from math import log
import numpy as np
import pandas as pd
from scipy import special


class Smooth():
    limit = 500

    def smooth(self, df, cols):
        return


class SmoothAvg(Smooth):
    def smooth(self, df, cols, target):
        for v in cols:
            imp = v[0]
            cl = v[1]
            alpa = df[cl].mean()
            beta = df[imp].mean()
            df[cl] = df[cl] + alpa
            df[imp] = df[imp] + beta

        return df


class SmoothwithBeta(Smooth):
    def smooth(self, df, cols):
        for v in cols:
            mean = df[v].mean()
            std = df[v].std()
            alpha = mean*(mean*(1-mean)/std-1)
            beta = (1-mean)*(mean*(1-mean)/std-1)
            subv = v[0:-3]
            df[v] = (df[subv+"cl"]+alpha)/(df[subv+"imp"]+alpha+beta)


class SmoothandRTBBeta(Smooth):
    def smooth(self, df, cols, impcl):
        imp = impcl[0]
        cl = impcl[1]
        for v in cols:
            gball = df.groupby(by=v).agg({imp: np.sum, cl: np.sum})
            gball = gball.reset_index()
            # alpa = gball[cl].mean()
            # beta = gball[imp].mean()
            alpa = 1
            beta = 20
            gb = gball[gball[imp] > 0]
            k = 0
            while True:

                gb['fenzi'] = gb[cl].apply(lambda x: special.digamma(x + alpa) - special.digamma(alpa))
                gb['fenzi1'] = gb.apply(lambda x: special.digamma(x[imp] - x[cl] + beta) - special.digamma(beta), axis=1)

                gb['fenmu'] = gb[imp].apply(lambda x: special.digamma(x + alpa + beta) - special.digamma(alpa + beta))
                fenmu = gb['fenmu'].sum()

                alpa = alpa * (gb['fenzi'].sum() / fenmu)
                beta = beta * (gb['fenzi1'].sum() / fenmu)
                k = k + 1
                print alpa, beta
                if k > 1000:
                    break

            gball[v + cl] = gball[cl] + alpa
            gball[v + imp] = gball[imp] + alpa + beta
            gball[v + 'ctr'] = gball[cl] / gball[imp]
            del gball[cl], gball[imp]
            df = pd.merge(df, gball, left_on=v, right_on=v, how='left')

        return df


if __name__ == '__main__':
    smooth = SmoothImpClBeta()
    df = pd.read_table('D:/1.tsv')
    df.describe()
    smooth.smooth(df, [['creativeid']], ['imp', 'cl'])
