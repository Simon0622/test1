#!/usr/bin/python
# coding=utf-8
import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow.python.framework import dtypes
from tensorflow.contrib import learn
from sklearn.metrics import mean_squared_error
import logging
import math

logging.basicConfig(level=logging.INFO)
# path = "/Users/simon/Downloads/didi/middata/"
# order = pd.read_csv(path+'ordermid.csv')

# mk = order.groupby(['ljminute','fromid']).agg({'success':np.sum,'cnt':np.sum,'minute':np.mean}).reset_index()
# mk = mk.sort_values('ljminute')

# ####随便赋的，因为开始几个并不重要
# last1 = 0
# last2 = 0
# last3 = 0
# last4 = 0
# last5 = 0
# minu = []
# min5 = []
# min10 = []
# min20 = []
# min30 = []
# x = []
# y = []
# suc = []

# queue = np.zeros(30)
# k = 1
# for index,row in mk[(mk.fromid==51)].iterrows():
#     if k<1440:
#         k = k+1
#     else:
#         x.append(row['ljminute'])
#         y.append(row['cnt'])
#         for i in range(len(queue)-1,0,-1):
#             queue[i] = queue[i-1]
#         queue[0] = row['cnt']

#         minu.append(row['minute'])
#         min5.append(queue[:5].sum()/5)
#         min10.append(queue[:10].sum()/10)
#         min20.append(queue[:20].sum()/20)
#         min30.append(queue.sum()/30)

# cntx = zip(minu,min5,min10,min20,min30)
# cnty = min10
# trainx = []
# trainy = []
LOG_DIR = './ops_logs'
TIMESTEPS = 4
RNN_LAYERS = [{'steps': TIMESTEPS,'keep_prob':0.9}]
DENSE_LAYERS = None
TRAINING_STEPS = 500000
BATCH_SIZE = 20
PRINT_STEPS = TRAINING_STEPS / 500

# trainx = []
# trainy = []
# for i in range(0,len(cnty)-TIMESTEPS):
#     tmpx = []
#     for j in range(0,TIMESTEPS):
#         tmpx.append(cntx[i+j])
#     trainx.append(tmpx)
#     trainy.append(cnty[i])

# for i in range(0,len(cntx)-TIMESTEPS-1):
#     tmpx = []
#     tmpy = []
#     for j in range(0,TIMESTEPS):
#         tmpx.append(cntx[i+j])
#     trainx.append(tmpx)
#     trainy.append(cnty[i+TIMESTEPS+1])


def x_sin(x):
    return x * np.sin(x)

def x2_y(x):
    return 3*x[:,0]+ 2*x[:,1]

def sin_cos(x):
    return pd.DataFrame(dict(a=np.sin(x), b=np.cos(x)), index=x)


def rnn_data(data, time_steps, labels=False):
    """
    creates new data frame based on previous observation
      * example:
        l = [1, 2, 3, 4, 5]
        time_steps = 2
        -> labels == False [[1, 2], [2, 3], [3, 4]]
        -> labels == True [2, 3, 4, 5]
    """
    rnn_df = []
    for i in range(len(data) - time_steps):
        if labels:
            try:
                rnn_df.append(data.iloc[i + time_steps].as_matrix())
            except AttributeError:
                rnn_df.append(data.iloc[i + time_steps])
        else:
            data_ = data.iloc[i: i + time_steps].as_matrix()
            rnn_df.append(data_ if len(data_.shape) > 1 else [[i] for i in data_])
    return np.array(rnn_df)


def split_data(data, val_size=0.1, test_size=0.1):
    """
    splits data to training, validation and testing parts
    """
    ntest = int(round(len(data) * (1 - test_size)))
    nval = int(round(len(data.iloc[:ntest]) * (1 - val_size)))

    df_train, df_val, df_test = data.iloc[:nval], data.iloc[nval:ntest], data.iloc[ntest:]

    return df_train, df_val, df_test


def prepare_data(data, time_steps, labels=False, val_size=0.1, test_size=0.1):
    """
    Given the number of `time_steps` and some data,
    prepares training, validation and test data for an lstm cell.
    """
    df_train, df_val, df_test = split_data(data, val_size, test_size)
    train = rnn_data(df_train, time_steps, labels=labels)
    val = rnn_data(df_val, time_steps, labels=labels)
    test = rnn_data(df_test, time_steps, labels=labels)
    return (train,val,test)


def generate_data(fct, x, time_steps, seperate=False):
    """generates data with based on a function fct"""
    data = fct(x)
    if not isinstance(data, pd.DataFrame):
        data = pd.DataFrame(data)
    train_x, val_x, test_x = prepare_data(data['a'] if seperate else data, time_steps)
    train_y, val_y, test_y = prepare_data(data['b'] if seperate else data, time_steps, labels=True)
    return dict(train=train_x, val=val_x, test=test_x), dict(train=train_y, val=val_y, test=test_y)


def lstm_model(time_steps, rnn_layers, dense_layers=None):

    def lstm_cells(layers):
        if isinstance(layers[0], dict):
            return [tf.nn.rnn_cell.DropoutWrapper(tf.nn.rnn_cell.BasicLSTMCell(layer['steps']),layer['keep_prob'])
                    if layer.get('keep_prob') else tf.nn.rnn_cell.BasicLSTMCell(layer['steps'])
                    for layer in layers]
        return [tf.nn.rnn_cell.BasicLSTMCell(steps) for steps in layers]

    def dnn_layers(input_layers, layers):
        if layers and isinstance(layers, dict):
            return learn.ops.dnn(input_layers,
                                 layers['layers'],
                                 activation=layers.get('activation'),
                                 dropout=layers.get('dropout'))
        elif layers:
            return learn.ops.dnn(input_layers, layers)
        else:
            return input_layers

    def _lstm_model(X, y):
        stacked_lstm = tf.nn.rnn_cell.MultiRNNCell(lstm_cells(rnn_layers))
        x_ = learn.ops.split_squeeze(1, time_steps, X)
        output, layers = tf.nn.rnn(stacked_lstm, x_, dtype=dtypes.float32)
        output = dnn_layers(output[-1], dense_layers)
        #cost =  learn.models.linear_regression(output, y)
        cost = tf.sqrt((y-tf.reduce_mean(output,1))**2)/(y+1)
        return tf.reduce_mean(output,1),tf.reduce_sum(cost)

    return _lstm_model


if __name__ == '__main__':
    regressor = learn.TensorFlowEstimator(model_fn=lstm_model(TIMESTEPS, RNN_LAYERS, [3,1]), n_classes=0,
                                          verbose=2,  steps=TRAINING_STEPS, optimizer='Adagrad',
                                          learning_rate=0.1, batch_size=BATCH_SIZE)
    X, y = generate_data(x2_y, np.random.rand(10000,2), TIMESTEPS, seperate=False)
    X, y = generate_data(np.sin, np.linspace(0, 100, 10000), TIMESTEPS, seperate=False)
    X = {}
    y = {}
    X['train'] = np.random.rand(1000,TIMESTEPS,2)
    y['train'] = np.random.rand(1000,)
    X['val'] = np.random.rand(100,TIMESTEPS,2)
    y['val'] = np.random.rand(100,)

    trainx = np.array(X['train'])
    trainy = np.array(y['train'])
    validation_monitor = learn.monitors.ValidationMonitor(X['val'], y['val'],print_steps=PRINT_STEPS,early_stopping_rounds=1000)
    regressor.fit(trainx, trainy, monitor=validation_monitor, logdir=LOG_DIR)

