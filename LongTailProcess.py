#!/usr/bin/python
#coding=utf-8
from __future__ import division
from math import log
import numpy as np
import pandas as pd


class LongTailProcess():
    def long_tail(self, df, cols):
        return


class LongTailProcessDorpNA(LongTailProcess):
    def long_tail(self, df, cols, target):
        # 处理各个维度的长尾值
        tgb = df.groupby(target).size()
        tgb = tgb.reset_index()
        tgb.rename(columns={0: 'size'}, inplace=True)
        # 求出总数
        tsum = tgb['size'].sum()
        tgb['pi'] = tgb['size'] / tsum
        l = [tgb['pi']]
        pargs = np.array(l, dtype=pd.Series)
        # 求出根节点的熵
        totalEnt = self.getEnt(pargs[0])
        # 求条件熵
        for col in cols:
            df['label'] = df[col]
            #条件熵，越小越好
            colcoEnt = self.computeConEnt(col, tsum, df, target)
            gb = df.groupby(col).size()
            length = len(gb)-1
            for i in range(0,length):
                #合并第一个和倒数第二个label一样
                gb = df.groupby('label').size()
                gb = gb.reset_index()
                gb.rename(columns={0:'size'},inplace=True)
                gb = gb.sort_values('size',ascending=True)
                gb = gb.reset_index()
                del gb['index']

                #将当前这个和他上一个的label合并
                df.label[df[col]==gb.ix[i,'label']] = gb.ix[i+1,'label']
                coEnt = self.computeConEnt('label', tsum, df, target)
                if(coEnt<colcoEnt):
                    colcoEnt = coEnt
                else:
                    df.label[df[col]==gb.ix[i,'label']] = df[col]
                    print col+" "+str(coEnt)
                    break




    def getEnt(self, pargs):
        sum = 0
        for i in pargs:
            sum += i * log(i, 2)
        return -sum

    def computeConEnt(self, col, tsum, df, target):
        # 求条件熵
        gb = df.groupby(by=[col, target]).size()
        gb = gb.reset_index()
        gb.rename(columns={0: 'size'}, inplace=True)
        #列出该列所有取值
        nodelist = df[col].unique().tolist()
        sumConEnt = 0
        for i in nodelist:
            gbi = gb[gb[col] == i]
            sum = gbi['size'].sum()
            pi = sum / tsum
            gbi['cpi'] = gbi['size'] / sum
            l = [gbi['cpi']]
            pargs = np.array(l, dtype=pd.Series)
            conEnt = self.getEnt(pargs[0])
            sumConEnt = sumConEnt + conEnt * pi
            pd.qcut()
        return sumConEnt

if __name__ == '__main__':
    longtail = LongTailProcessDorpNA()
    df = pd.read_table('D:/3hour_jtmodel.tsv.bak')
    longtail.long_tail(df,['width','height'],'fx')

